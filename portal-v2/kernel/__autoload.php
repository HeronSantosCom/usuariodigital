<?php

if (!function_exists("__autoload")) {

    /**
     * Inclui o arquivo contendo a classe instanciada
     * @author Heron Santos
     * @copyright www.heronsantos.com
     * @param string $class_name <p>Nome da classe a ser instanciada</p>
     * @return void
     */
    function __autoload($class_name) {
        if (!class_exists($class_name)) {
            $class_name = strtolower($class_name);
            if (file_exists("kernel/{$class_name}.php")) {
                include "kernel/{$class_name}.php";
            } elseif (file_exists("resource/{$class_name}/{$class_name}.php")) {
                include "resource/{$class_name}/{$class_name}.php";
            } else {
                include "application/{$class_name}.php";
            }
        }
    }

}
?>