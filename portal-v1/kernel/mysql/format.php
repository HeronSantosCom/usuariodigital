<?php

class MySQLFormat {

    static public function Ereaser($value, $capsulate = false, $exception = true) {
        $function = self::IsFunction($value);
        if ($function) {
            if ($capsulate and $exception) {
                trigger_error("Não é possível capsular a função {$function}.", E_USER_WARNING);
                return false;
            }
        } else {
            $value = trim(str_replace(array('"', "'", "`"), "", $value));
            if (strlen($value) > 0 and $capsulate) {
                $value = "{$capsulate}{$value}{$capsulate}";
            }
        }
        return $value;
    }

    static public function IsFunction($value) {
        $position = trim(strpos($value, "("));
        $function = strtoupper(trim(substr($value, 0, $position)));
        switch ($function) {
            case "COUNT":
            case "AVG":
            case "CONCAT":
            case "CONCAT_WS":
            case "IF":
            case "MAX":
            case "MIN":
            case "MD5":
            case "DATE":
            case "DATE_FORMAT":
            case "LENGTH":
                return $function;
                break;
        }
        return false;
    }

}

?>
