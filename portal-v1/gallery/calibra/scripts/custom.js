//jQuery.noConflict();




function playerFades() {


			//jQuery("#zoom").css({'opacity':'0.7'});


			jQuery("a[rel^='prettyPhoto']").prettyPhoto();

			jQuery("#resumeButton").fadeTo("slow", 0.4);


			jQuery("#resumeButton").click(function(){
			jQuery(this).stop().fadeTo("medium", 0.4);
			jQuery("#pauseButton").stop().fadeTo("slow", 1.0);
			});


		   jQuery(".zoom").css({'opacity':'0'});
			jQuery(".preloader, .preloader_visible,.image-wrap").hover(function(){
			jQuery("span",this).show();
			jQuery(this,"img").stop().fadeTo("medium", 0.6);
			jQuery(".zoom",this).stop().fadeTo("medium", 1);
			},function(){
			jQuery(this,"img").stop().fadeTo("medium", 1);
			jQuery("span",this).hide();

			});


			jQuery("#pauseButton").click(function(){
			jQuery(this).stop().fadeTo("medium", 0.4);
			jQuery("#resumeButton").stop().fadeTo("slow", 1.0);
			});


			jQuery("#nav li a span").css({'opacity':'0.6'});
			jQuery("#nav li a span").hover(function(){
			jQuery(this).stop().fadeTo("medium", 0);
			//jQuery(this).stop().animate({top:'-48px'},{queue:false,duration:200});
			},function(){
			jQuery(this).stop().fadeTo("fast", 0.6);
			//jQuery(this).stop().animate({top:'0px'},{queue:false,duration:100});
			});


			jQuery('.slided span').show();
			jQuery(".slided span").stop().fadeTo(0, 0);



			};

jQuery(document).ready(function(){

		//  Demo only


        jQuery('#acc-radio_0').click(
            function() {
                  jQuery("#kwicks").show();
            }
        );

		   jQuery('#acc-radio_1').click(
            function() {
                  jQuery("#kwicks").hide();
            }
        );

		 Cufon.replace('h1,h2,h3, h3 a',{});

		jQuery('#ajaxcontent #postPagination a').live('click', function(e){
		e.preventDefault();
		var link = jQuery(this).attr('href');
		jQuery('#ajaxcontent').fadeOut(200,stopCycle).load(link + ' #ajaxloader',
		function(){jQuery('#ajaxcontent').fadeIn(200,Cufon.refresh), hoverbutton(); startCycle(); playerFades();prettyPhoto.init(); } );
		});

		jQuery('#gallerycycle').cycle({
        fx: 'fade',
        speed:300,
		  easing: 'easeInOutQuad',
		  cleartype:  1,
		  pause:0,
        timeout: 0,
        pager:  '#gallerynav'


		});

		var home_slider_effect = jQuery("meta[name=home_slider_effect]").attr('content');
		var home_slider_timeout = jQuery("meta[name=home_slider_timeout]").attr('content');

		 jQuery('#slideshow div:first').fadeIn(1000, function() {
		jQuery('#slideshow').cycle({
        fx:home_slider_effect,
        speed:300,
		  easing: 'easeInOutQuad',
		  cleartype:  1,
		  pause:0,
        timeout: home_slider_timeout,
        next: '.slideshow-next',
		  prev: '.slideshow-prev'
		});
		});

			startCycle();

			playerFades();

			hoverbuttonReverse();



			jQuery("#resumeButton, #pauseButton, .zoomButton, .showTip, .social li a, #homebutton a, .styleswitch").simpletooltip();


			jQuery("a[rel^='prettyPhoto']").prettyPhoto();

			jQuery(".social li a").css({'opacity':'0.7'});

			// Adds hover-function for buttons exept on IE
			if(!jQuery.browser.msie){
					hoverbutton();
				}


			jQuery("#primary-menu ul li.homelink a").css({'opacity':'0.6'});
			jQuery("#primary-menu ul li.homelink a").hover(function(){
			jQuery(this).stop().fadeTo("slow", 0.8);
			},function(){
			jQuery(this).stop().fadeTo("slow", 0.6);
			});

			jQuery("#s").css({'opacity':'0'});
			jQuery("#searchform #searchsubmit, #s:hidden ").hover(function(){
			jQuery(".socialmedia").hide();
			jQuery("#s").show().stop().fadeTo("slow", 1);
			});

			jQuery("#searchform").hover(function(){
			},function(){
			jQuery("#s").stop().fadeTo("slow", 0).hide();
			jQuery(".socialmedia").show();

			});



			jQuery("#homebutton a").css({'opacity':'0.9'});
			jQuery("#homebutton a").hover(function(){
			jQuery(this).stop().fadeTo("slow", 1);
			},function(){
			jQuery(this).stop().fadeTo("slow", 0.9);
			});


	});


function hoverbutton(){
		jQuery(".btn, #submitbutton, #submit").hover(function(){
			jQuery(this).stop().fadeTo("medium", 0.6);
			},function(){
			jQuery(this).stop().fadeTo("slow", 1.0);
			});
}

function hoverbuttonReverse(){
		jQuery(".clearborder").hover(function(){
			jQuery(this).stop().fadeTo("fast", 1);
			},function(){
			jQuery(this).stop().fadeTo("medium", 0.7);
			});

}




			var i = 0;//initialize
			var int=0;//Internet Explorer Fix
			jQuery(window).bind("load", function() {//The load event will only fire if the entire page or document is fully loaded
			var int = setInterval("doThis(i)",200);//500 is the fade in speed in milliseconds
			});

			function doThis() {
			var images = jQuery('img').length;//count the number of images on the page
			if (i >= images) {// Loop the images
			clearInterval(int);//When it reaches the last image the loop ends
			}
			jQuery('.preloader img:hidden').eq(0).fadeIn(500);//fades in the hidden images one by one
			i++;//add 1 to the count
	}


// Reverses the z-indexing for correcting ie7 z-index issues
/*jQuery(function() {
	var zIndexNumber = 1000;
	jQuery('div').each(function() {
		jQuery(this).css('zIndex', zIndexNumber);
		zIndexNumber -= 10;
	});
});*/





// JavaScript Document

/*
 * Cross-browser event handling, by Scott Andrew
 */
function addEvent(element, eventType, lamdaFunction, useCapture) {
    if (element.addEventListener) {
        element.addEventListener(eventType, lamdaFunction, useCapture);
        return true;
    } else if (element.attachEvent) {
        var r = element.attachEvent('on' + eventType, lamdaFunction);
        return r;
    } else {
        return false;
    }
}


/*
 * Clear Default Text: functions for clearing and replacing default text in
 * <input> elements.
 *
 * by Ross Shannon, http://www.yourhtmlsource.com/
 */

addEvent(window, 'load', init, false);

function init() {
    var formInputs = document.getElementsByTagName('input');
    for (var i = 0; i < formInputs.length; i++) {
        var theInput = formInputs[i];

        if (theInput.type == 'text' && theInput.className.match(/\bcleardefault\b/)) {
            /* Add event handlers */
            addEvent(theInput, 'focus', clearDefaultText, false);
            addEvent(theInput, 'blur', replaceDefaultText, false);

            /* Save the current value */
            if (theInput.value != '') {
                theInput.defaultText = theInput.value;
            }
        }
    }
}

function clearDefaultText(e) {
    var target = window.event ? window.event.srcElement : e ? e.target : null;
    if (!target) return;

    if (target.value == target.defaultText) {
        target.value = '';
    }
}

function replaceDefaultText(e) {
    var target = window.event ? window.event.srcElement : e ? e.target : null;
    if (!target) return;

    if (target.value == '' && target.defaultText) {
        target.value = target.defaultText;
    }
}

/**
*
*	simpleTooltip jQuery plugin, by Marius ILIE
*	visit http://dev.mariusilie.net for details
*
**/
(function(jQuery){ jQuery.fn.simpletooltip = function(){
	return this.each(function() {
		var text = jQuery(this).attr("title");
		jQuery(this).attr("title", "");
		if(text != undefined) {
			jQuery(this).hover(function(e){
				var tipX = e.pageX + 12;
				var tipY = e.pageY + 12;
				jQuery(this).attr("title", "");
				jQuery("body").append("<div id='simpleTooltip' style='position:absolute; z-index: 9999; display: none;'>" + text + "</div>");
				if(jQuery.browser.msie) var tipWidth = jQuery("#simpleTooltip").outerWidth(true)
				else var tipWidth = jQuery("#simpleTooltip").width()
				jQuery("#simpleTooltip").width(tipWidth);
				jQuery("#simpleTooltip").css("left", tipX).css("top", tipY).fadeIn("medium");
			}, function(){
				jQuery("#simpleTooltip").remove();
				jQuery(this).attr("title", text);
			});
			jQuery(this).mousemove(function(e){
				var tipX = e.pageX - 10;
				var tipY = e.pageY - 40;
				var tipWidth = jQuery("#simpleTooltip").outerWidth(true);
				var tipHeight = jQuery("#simpleTooltip").outerHeight(true);
				if(tipX + tipWidth > jQuery(window).scrollLeft() + jQuery(window).width()) tipX = e.pageX - tipWidth;
				if(jQuery(window).height()+jQuery(window).scrollTop() < tipY + tipHeight) tipY = e.pageY - tipHeight;
				jQuery("#simpleTooltip").css("left", tipX).css("top", tipY).fadeIn("medium");
			});
		}
	});
}})(jQuery);

jQuery(function() {
	jQuery('a[rel*=external]').click( function() {
		window.open(this.href);
		return false;
	});
});

function stopCycle() {

    jQuery('#cycle').cycle({

        timeout:0,
 			pause:1
	 });

  };

function startCycle() {

		var $slider_effect = jQuery("meta[name=slider_effect]").attr('content');
		var $slider_timeout = jQuery("meta[name=slider_timeout]").attr('content');

    jQuery('#cycle img:first').fadeIn(1000, function() {
    jQuery('#cycle').cycle({
        fx: $slider_effect,
        speed:300,
		  easing: 'easeInOutQuad',
		  cleartype:  1,
		  pause:1,
        timeout: $slider_timeout,
        pager:  '#nav',
        pagerAnchorBuilder: function(idx, slide) {
        // return selector string for existing anchor
        return '#nav li:eq(' + idx + ') a';

    }


    });

	 jQuery('#pauseButton').click(function() {
    jQuery('#cycle').cycle('pause');
	});

	 jQuery('#resumeButton').click(function() {
    jQuery('#cycle').cycle('resume');
	});


	 });

  };

