<?php

if (!class_exists('mysqlshow')) {

    /**
     * Fornecem informações sobre bancos de dados, tabelas, colunas ou informações sobre o status do servidor na base de dados MySQL.
     * @name kengoo/kernel/mysql/mysqlshow
     * @author Heron Santos
     * @todo dividir os indices em array, contendo os itens e alias
     * @copyright www.heronsantos.com
     */
    class mysqlshow extends mysql {

        private $database = false;
        private $table = false;
        private $index = false;

        public function __construct() {
            parent::__construct();
        }

        /**
         * Seta o banco de dados.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $name <p>Bando de dados</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function database($name) {
            $index = $this->index("database");
            $this->database[$index] = $this->ereaser($name, "`");
            return $this;
        }

        /**
         * Seta a tabela.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $name <p>Tabela</p>
         * @param integer $databasekey <p>Banco de dados (chave de indicação), padrão = 1</p>
         * @param string $alias <p>Apelido para tabela, padrão = FALSE</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function table($name, $databasekey = 1, $alias = false) {
            $database = (isset($this->database[$databasekey]) ? "{$this->database[$databasekey]}." : $this->ereaser($this->select, "`") . ".");
            $table = $this->ereaser($name, "`");
            $index = $this->index("table");
            $this->table[$index] = array(false, "{$database}{$table}");
            return $this;
        }

        /**
         * Exibe informações sobre as colunas em uma determinada tabela.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $name <p>Coluna</p>
         * @param integer $tablekey <p>Tabela (chave de indicação), padrão = FALSE</p>
         * @param string $alias <p>Apelido para coluna, padrão = FALSE</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function columns() {
            if ($this->table) {
                $search = false;
                foreach ($this->table as $value) {
                    $result = $this->commit("SHOW /*!32332 FULL */ COLUMNS FROM {$value[1]}");
                    if ($result["result"]) {
                        if (@mysql_num_rows($result["result"]) > 0) {
                            while ($line = mysql_fetch_assoc($result["result"])) {
                                $search[] = $line;
                            }
                            mysql_free_result($result["result"]);
                        }
                    }
                }
                return $search;
            }
            trigger_error("Table unreported!");
            return false;
        }

        /**
         * Define ou retorna o ponteiro de um determinado objeto.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $object <p>Objeto</p>
         * @param bool $get <p>TRUE retorna o ponteiro atual do objeto, padrão = FALSE</p>
         * @return integer <p>Ponteiro, caso contrario FALSE</p>
         */
        protected function index($object, $get = false) {
            if ($get) {
                if (isset($this->index[$object])) {
                    return $this->index[$object];
                }
                return false;
            }
            return $this->index[$object] = (isset($this->index[$object]) ? $this->index[$object] + 1 : 1);
        }

    }

}
?>