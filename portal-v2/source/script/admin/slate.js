var slate = {};

slate = function () {
    var pub = {};
    var self = {};
    var chartColors = ['#263849','#FF9900','#555','#777','#999','#bbb','#ccc','#eee'];

    pub.init = function () {
        
        $('#search').find ('input').live ('click' , function () {
            $(this).val ('')
        });
        $("form.form select, form.form input:checkbox, form.form input:radio, form.form input:file").uniform();

        var dataTablei28n = {
            "sProcessing":   "Processando...",
            "sLengthMenu":   "Mostrar _MENU_ registros",
            "sZeroRecords":  "Não foram encontrados resultados",
            "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registros",
            "sInfoFiltered": "(filtrado de _MAX_ registros no total)",
            "sInfoPostFix":  "",
            "sSearch":       "Buscar:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "Primeiro",
                "sPrevious": "Anterior",
                "sNext":     "Seguinte",
                "sLast":     "Último"
            }
        };

        $('*[rel=datatable]').dataTable ({
            "oLanguage": dataTablei28n
        });

        $('#postagens').dataTable ({
            "bProcessing": true,
            "sAjaxSource": 'json/postagens.php',
            "sPaginationType": "full_numbers",
            "bStateSave": true,
            "oLanguage": dataTablei28n
        });

        /* Add events */
        $('#postagens tbody tr').live('dblclick', function () {
            var nTds = $('td', this);
            var sID = $(nTds[0]).text();
            //var sBrowser = $(nTds[1]).text();
            //var sGrade = $(nTds[4]).text();
            window.open("painel.html?content=postagens-editar&id=" + sID, "_self");
            alert(sBrowser);
        } );

        $("*[rel=tooltip]").tipsy ({
            gravity: 's'
        });
        $("*[rel=facebox]").facebox ();

        $('table.stats').each(function() {
            var chartType = '';

            if ( $(this).attr('title') ) {
                chartType = $(this).attr('title');
            } else {
                chartType = 'area';
            }

            var chart_width = $(this).parents ('.portlet').width () * .85;

            $(this).hide().visualize({
                type: chartType,	// 'bar', 'area', 'pie', 'line'
                width: chart_width,
                height: '240px',
                colors: chartColors
            });
        });
    }

    return pub;

}();