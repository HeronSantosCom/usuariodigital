<?php

class Account {

    private $ids, $search, $options;
    private $page = 1;

    public function __construct() {
        Index::MySQL();
        System::Set("Title", "Contas de usuário");
        if (isset($_GET["options"])) {
            $this->options = $_GET["options"];
        }
    }

    public function SetId($id = false) {
        return $this->ids = ($id ? $id : Permalink::Get(1));
    }

    public function SetPage($page = false) {
        return $this->page = ($page ? $page : (Permalink::Get(1) ? Permalink::Get(1) : 1));
    }

    public function SetSearch($search = false) {
        if (isset($_POST["search"])) {
            $this->search = $_POST["search"];
            $this->page = 1;
            System::Set("Redirect", "admin/internal/config/account/1/{$this->search}", "Admin");
            return true;
        }
        return $this->search = ($search ? $search : Permalink::Get(2));
    }

    public function My() {
        System::Set("Title", "Minha conta");
        return $this->SetId(System::Get("id", "Authenticate"));
        if (isset($_POST["ips_confirm"])) {
            $_POST["ips_confirm"]["status"] = "1";
        }
    }

    public function Lock() {
        System::Set("History", str_replace("/admin/internal", "/admin", $_SERVER['REQUEST_URI']), "Admin");
        if (!System::Get("id", "Authenticate")) {
            if (!System::Get("OIS-Ajax")) {
                System::Redirect(Core::Remote("admin/login"));
                return true;
            }
            Admin::Go(Core::Remote("admin/login"));
            return true;
        } else {
            System::Clean("Form");
            System::Set("Redirect", "dashboard");
            if (Permalink::Get()) {
                System::Set("Redirect", join("/", Permalink::Get()));
            }
        }
        return false;
    }

    public function Save() {
        System::Set("status", "1", "Form");
        if (isset($_POST["ips_confirm"])) {
            $this->Clean();
            System::Extract($_POST, false, false, "Form");
            $saved = (!$this->ids ? $this->Insert() : $this->Update($this->ids));
            if ($saved) {
                System::Redirect(Core::Remote(System::Get("Redirect", "Admin")));
                Admin::Alert("Registro salvado!");
                return true;
            }
            Admin::Alert("Problemas ao efetuar operação!");
        }
        return false;
    }

    public function Delete() {
        if ($this->ids) {
            $delete = new MySQLDelete();
            $delete->Table("ips_account");
            $delete->In("id", explode(",", $this->ids));
            if ($delete->Go()) {
                System::Redirect(Core::Remote(System::Get("Redirect", "Admin")));
                Admin::Alert("Registro(s) removido(s)!");
                return true;
            }
        }
        Admin::Alert("Problemas ao efetuar operação!");
        return false;
    }

    public function Open() {
        if ($this->ids) {
            $search = new MySQLSearch();
            $search->Table("ips_account");
            $search->Column("*");
            $search->Match("id", $this->ids);
            $search->Limit(1);
            $search = $search->Go();
            if ($search) {
                System::Extract($search[0], null, null, "Form");
                return true;
            }
            Admin::Alert("Registro não encontrado!");
        }
        return false;
    }

    public function Search() {
        $search = new MySQLSearch();
        $search->Table("ips_account");
        $search->Column("id");
        $search->Column("name");
        $search->Column("account");
        $search->Column("status");
        $search->Column("DATE_FORMAT(`insert`, '%d/%m/%Y %H:%i')", false, "insert_date");
        if ($this->search) {
            $search->Like("name", "%{$this->search}%");
            $search->Like("account", "%{$this->search}%", "OR");
            $search->Like("DATE_FORMAT(`insert`, '%d/%m/%Y %H:%i')", "%{$this->search}%", "OR");
            System::Set("Search Text", $this->search);
        }
        $search->Order(2);
        $search->Order(3);
        $search->Order(4);
        $search->limit = $this->Pagination($search->where);
        $search = $search->Go();
        if ($search) {
            return System::Set("Search", $search);
        }
        return false;
    }

    public function Options() {
        $search = new MySQLSearch();
        $search->Table("ips_account");
        $search->Column("id");
        $search->Column("name");
        $search->Order(2);
        if ($this->options) {
            $search->Like("name", "%{$this->options}%");
        }
        System::Set("Options Account", $search->Go());
    }

    private function Pagination($where) {
        $total = new MySQLSearch();
        $total->Table("ips_account");
        $total->Column("COUNT(id)", false, "total");
        $total->where = $where;
        $pagination = System::Pages($total, $this->page, admin_limit);
        if ($pagination) {
            System::Set("Search Pagination", $pagination[1]);
            return $pagination[0];
        }
        return false;
    }

    private function Clean() {
        $clean = new MySQLShow();
        $clean->Table("ips_account");
        $fields = $clean->Columns();
        if ($fields) {
            foreach ($fields as $field) {
                System::Set($field["Field"], false, "Form");
            }
        }
    }

    private function Insert() {
        $save = new MySQLSave();
        $save->Table("ips_account");
        $save->Column("name", System::Get("name", "Form"));
        $save->Column("account", System::Get("account", "Form"));
        $save->Column("status", System::Get("status", "Form"));
        $save->Column("password", md5(System::Get("password", "Form")));
        $save->Column("insert", date("Y-m-d H:i:s"));
        if ($save->Go()) {
            return $save->GetID();
        }
        return false;
    }

    private function Update($id) {
        $save = new MySQLSave();
        $save->Table("ips_account");
        $save->Column("name", System::Get("name", "Form"));
        $save->Column("account", System::Get("account", "Form"));
        $save->Column("status", System::Get("status", "Form"));
        if (strlen(System::Get("password", "Form")) > 0) {
            $save->Column("password", md5(System::Get("password", "Form")));
        }
        $save->Match("id", $id);
        if ($save->Go()) {
            return $id;
        }
        return false;
    }

    static function Login() {
        if (!System::Get("id", "Authenticate")) {
            if (isset($_POST["account"])) {
                extract($_POST);
                $search = new MySQLSearch();
                $search->Table("ips_account");
                $search->Column("*");
                $search->Match("account", $account);
                $search->Match("password", md5($password), "AND");
                $search->Match("status", "1", "AND");
                $search->Limit(1);
                $search = $search->Go();
                if ($search) {
                    System::Set("Redirect", "admin/internal/dashboard", "Admin");
                    System::Extract($search[0], null, null, "Authenticate");
                    System::Redirect(System::Get("History", "Admin"));
                    return true;
                }
                Admin::Alert("Nome ou senha do usuário inválido!");
            }
        } else {
            System::Redirect(Core::Remote("admin"));
        }
        if (!System::Get("History", "Admin")) {
            System::Set("History", str_replace("/admin/internal", "/admin", $_SERVER['REQUEST_URI']), "Admin");
        }
        return false;
    }

    static function Logoff() {
        if (System::Get("id", "Authenticate")) {
            System::Clean("Authenticate");
            System::Clean("Admin");
            System::Redirect(Core::Remote("admin/login"));
            return true;
        }
        System::Redirect(Core::Remote("admin"));
        return false;
    }

}

?>