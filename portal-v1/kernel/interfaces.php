<?php

/**
 * Classe de carregamendo da interface
 */
class Interfaces extends System {

    /**
     * Exibe a interface a ser carregada de acordo com as requisições
     * @param string [optional] $path <p>Modulo a ser carregado!</p>
     * @param bool [optional] $internal
     */
    static function Show($path = false, $internal = false) { // $type = false,
        $type = "web";
        $permalink = Permalink::Get();
        if ($path) {
            $permalink = explode("/", $path);
        }
        $path = "interfaces/";
        if (is_array($permalink)) {
            $reset = 0;
            foreach ($permalink as $key => $arg) {
                if (!file_exists(Core::Local("{$path}/{$arg}/index.html"))) {
                    break;
                }
                $reset = $key + 1;
                $path = "{$path}/{$arg}";
            }
            Permalink::Reset($reset);
        }
        $open = "{$path}/index.html";
        parent::StopLooping($open);
        return self::Explore((file_exists($open) ? file_get_contents($open) : false), $internal);
    }

    /**
     * Formata conteúdo a ser executado
     * @param string $container
     * @param bool [optional] $internal
     * @return string
     */
    static function Explore($container, $internal = false) {
        if (system_interface_compressed) {
            $container = str_replace("\r", "", $container);
            $container = str_replace("\n", "", $container);
            $container = str_replace("\t", "", $container);
            $container = str_replace("  ", "", $container);
        }
        $container = Source::Explore(trim($container));
        if (!$internal and !parent::Get("OIS-Ajax")) {
            if ($container) {
                if (system_interface_w3c_certified) {
                    $dom = new DOMDocument(null, system_interface_encoding);
                    $dom->formatOutput = true;
                    $dom->preserveWhiteSpace = false;
                    $dom->loadHTML(preg_replace('/^.*\n\n/s', '', $container, 1));
                    $container = $dom->saveHTML();
                }
            }
        }
        return $container;
    }

}

?>
