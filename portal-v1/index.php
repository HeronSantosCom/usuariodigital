<?php

/* !
 * Insign Platform System v1.4.2
 * http://www.insigndigital.com.br/ips/
 *
 * Copyright 2011, Heron Reis dos Santos
 *
 * Date: Tue Jan 11 08:52:48 2011 -0300
 */

define("ips_version", "1.2");
define("ips_name", "IPS");
define("ips_fullname", ips_name . " " . ips_version);

require_once 'kernel/system/__autoload.php';
require_once 'kernel/system/reporting.php';
require_once 'kernel/system/bash.php';
require_once 'kernel/system/config.php';
require_once 'kernel/system/pages.php';
require_once 'kernel/system/sqlinjection.php';

if (file_exists(Core::Local("ips.xml"))) {
    $object = @simplexml_load_file(Core::Local("ips.xml"));
    if (is_object($object)) {
        config($object);
    }
}

if (!defined("system_name")) {
    define("system_name", ips_fullname);
}

if (!defined("system_encoding")) {
    define("system_encoding", "utf8");
}

if (!defined("system_timezone_locale")) {
    define("system_timezone_locale", "pt_BR");
}

if (!defined("system_error_die")) {
    define("system_error_die", true);
}

if (!defined("system_error_show")) {
    define("system_error_show", true);
}

if (!defined("system_error_interface")) {
    define("system_error_interface", "error");
}

if (!defined("system_timezone_unix")) {
    define("system_timezone_unix", "America/Sao_Paulo");
}

if (!defined("system_timezone_gmt")) {
    define("system_timezone_gmt", "-3:00");
}

if (!defined("system_interface_encoding")) {
    define("system_interface_encoding", "utf-8");
}

if (!defined("system_interface_limit")) {
    define("system_interface_limit", "10");
}

if (!defined("system_interface_compressed")) {
    define("system_interface_compressed", true);
}

if (!defined("system_interface_w3c_certified")) {
    define("system_interface_w3c_certified", true);
}

header("Content-type: text/html; charset=" . system_encoding);
error_reporting(1);
ini_set('error_reporting', E_ALL);
ini_set("display_errors", 1);
set_error_handler('reporting');
register_shutdown_function('reporting');
setlocale(LC_ALL, system_timezone_locale);
date_default_timezone_set(system_timezone_unix);
session_start();
ob_start();

System::Clean();

try {
    print Interfaces::Show();
} catch (Exception $exc) {
    reporting($exc->getCode(), $exc->getMessage());
}

exit(ob_get_clean());
?>