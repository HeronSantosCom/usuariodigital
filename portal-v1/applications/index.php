<?php

class Index {

    public function __construct() {
        self::MySQL();
        System::Set("Site Mensagem", "Internet além do limite!");
        System::Set("Site Autor", "Heron Santos");
        System::Set("Site Logo", "{% url (gallery/calibra/img/shared/logo.png) %}");
        System::Set("Site URL", "{% url () %}");
        System::Set("Site Copyright", "Copyright " . date("Y") . " - {% define (ips_fullname) %}. Todos os direitos reservados.");
        $permalink = Permalink::Get();
        System::Set("Parent Permalink", $permalink);
        System::Set("Permalink", "internal");
        if ($permalink) {
            System::Set("Permalink", "internal/" . join("/", $permalink));
        }
    }

    public function Social() {
        $Array = false;
        $Array[] = array("id" => "facebook", "name" => "Facebook", "link" => "http://www.facebook.com/");
        $Array[] = array("id" => "blogger", "name" => "Blogger", "link" => "http://www.blogger.com/");
        $Array[] = array("id" => "flickr", "name" => "Flickr", "link" => "http://www.flickr.com/");
        $Array[] = array("id" => "linkedin", "name" => "LinkedIn", "link" => "http://www.linkedin.com/");
        $Array[] = array("id" => "myspace", "name" => "MySpace", "link" => "http://www.myspace.com/");
        $Array[] = array("id" => "twitter", "name" => "Twitter", "link" => "http://www.twitter.com/");
        return System::Set("Social", $Array);
    }

    public function Menu() {
        $search = new MySQLSearch();
        $search->Table("ips_category");
        $search->Column("id");
        $search->Column("name");
        $search->Column("LENGTH(name)", false, "len");
        $search->Group(2);
        $search->Having(3, "15", "<=");
        $search->Limit(5);
        return System::Set("Menu", $search->Go());
    }

    public function Categorias() {
        $search = new MySQLSearch();
        $search->Table("ips_category");
        $search->Column("id");
        $search->Column("name");
        $search->Order(2);
        return System::Set("Categorias", $search->Go());
    }

    public function Initial() {
        $search = new MySQLSearch();
        $search->Table("ips_post");
        $search->Join("ips_account", array("ips_account_id", "=", "id"), "LEFT");
        $search->Column("id");
        $search->Column("title");
        $search->Column("text");
        $search->Column("DATE_FORMAT(`t1`.`insert`, '%d/%m/%Y %H:%i')", false, "insert");
        $search->Column("id", 2, "ips_account_id");
        $search->Column("name", 2, "ips_account_name");
        $search->Match("status", "1");
        $search->Order(4, "DESC");
        $search->Limit(system_interface_limit);
        $tract = $search->Go();
        if (is_array($tract)) {
            foreach ($tract as $key => $post) {
                $aux = explode("<!-- pagebreak -->", $post["text"], 2);
                $tract[$key]["text"] = html_entity_decode(strip_tags($aux[0]), ENT_COMPAT, system_interface_encoding);
                $tract[$key]["ips_gallery_id"] = false;
                $tract[$key]["ips_gallery_title"] = false;
                $tract[$key]["ips_gallery_thumb"] = false;
                $gallery = new MySQLSearch();
                $gallery->Table("ips_gallery_has_ips_post");
                $gallery->Join("ips_gallery", array("ips_gallery_id", "=", "id"), "LEFT");
                $gallery->Column("id", 2);
                $gallery->Column("title", 2);
                $gallery->Column("thumb", 2);
                $gallery->Match("ips_post_id", $post["id"], false, false);
                $gallery->Match("status", "1", "AND", false, 2);
                $gallery->Limit(1);
                $thumb = $gallery->Go();
                if (isset($thumb[0])) {
                    $tract[$key]["ips_gallery_id"] = $thumb[0]["id"];
                    $tract[$key]["ips_gallery_title"] = $thumb[0]["title"];
                    $tract[$key]["ips_gallery_thumb"] = $thumb[0]["thumb"];
                }
            }
        }
        return System::Set("Search", $tract);
    }

    public function ViewPost() {
        $permalink = System::Get("Parent Permalink");
        if (isset($permalink[2])) {
            $search = new MySQLSearch();
            $search->Table("ips_post");
            $search->Join("ips_category", array("ips_category_id", "=", "id"), "LEFT");
            $search->Join("ips_account", array("ips_account_id", "=", "id", 1), "LEFT");
            $search->Column("*");
            $search->Column("id", 2, "ips_category_id");
            $search->Column("name", 2, "ips_category_name");
            $search->Column("id", 3, "ips_account_id");
            $search->Column("name", 3, "ips_account_name");
            $search->Match("id", $permalink[2], false, false);
            $search->Match("status", "1", "AND");
            $search = $search->Go();
            if (isset($search[0])) {
                $gallery = new MySQLSearch();
                $gallery->Table("ips_gallery_has_ips_post");
                $gallery->Join("ips_gallery", array("ips_gallery_id", "=", "id"), "LEFT");
                $gallery->Column("id", 2);
                $gallery->Column("title", 2);
                $gallery->Column("file", 2);
                $gallery->Column("thumb", 2);
                $gallery->Column("description", 2);
                $gallery->Match("ips_post_id", $search[0]["id"], false, false);
                $gallery->Match("status", "1", "AND", false, 2);
                System::Set("Gallery", $gallery->Go());
                System::Extract($search[0], false, false, "Post");
            }
        }
    }

    public function SearchCategory() {
        $search = new MySQLSearch();
        $search->Table("ips_category");
        $search->Column("id");
        $search->Column("name");
        $search->Column("description");
        $search->Order(2);
        $search->limit = $this->SearchPagination("ips_category", $search->where);
        return System::Set("Search", $search->Go());
    }

    public function SearchPost() {
        $search = new MySQLSearch();
        $search->Table("ips_post");
        $search->Column("id");
        $search->Column("title");
        $search->Column("text");
        $search->Match("status", "1");
        $search->Order(2);
        $search->limit = $this->SearchPagination("ips_post", $search->where);
        return System::Set("Search", $search->Go());
    }

    private function SearchPagination($table, $where) {
        $permalink = System::Get("Parent Permalink");
        $total = new MySQLSearch();
        $total->Table($table);
        $total->Column("COUNT(id)", false, "total");
        $total->where = $where;
        $pagination = System::Pages($total, (isset($permalink[1]) ? $permalink[1] : 1), admin_limit);
        if ($pagination) {
            System::Set("Search Pagination", $pagination[1]);
            return $pagination[0];
        }
        return false;
    }

    public function Novidades() {
        $search = new MySQLSearch();
        $search->Table("ips_post");
        $search->Column("id");
        $search->Column("title");
        $search->Column("DATE_FORMAT(`insert`, '%d/%m/%Y %H:%i')", false, "insert_date");
        $search->Match("status", "1");
        $search->Order(3, "DESC");
        $search->Limit(5);
        return System::Set("Novidades", $search->Go());
    }

    static function MySQL() {
        new MySQL(mysql_server, mysql_username, mysql_password, mysql_database, mysql_limit, mysql_global, system_timezone_unix, system_encoding);
    }

}

?>