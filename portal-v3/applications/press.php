<?php

class press extends control {

    public function __construct() {
        $this->extract($_GET);
    }

    public function content($content = "tag") {
        if (isset($this->content)) {
            $content = $this->content;
        }
        $this->slider = $this->html("index-slider.xhtml");
        $this->menu = $this->html("index-menu.xhtml");
        $this->popular = $this->html("index-popular.xhtml");
        $this->sites = $this->html("index-sites.xhtml");
        $this->content = $this->html("content-{$content}.xhtml");
    }

}

?>