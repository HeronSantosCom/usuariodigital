<?php

if (!function_exists("sqlinjection")) {

    /**
     * Sistema de anti SQL injection
     * @return null
     */
    function sqlinjection() {
//        $_GET = array_map('trim', $_GET);
//        $_POST = array_map('trim', $_POST);
//        $_REQUEST = array_map('trim', $_REQUEST);
        if (get_magic_quotes_gpc ()) {
            $_GET = array_map('stripslashes', $_GET);
            $_POST = array_map('stripslashes', $_POST);
            $_REQUEST = array_map('stripslashes', $_REQUEST);
        }
//        $_GET = array_map('mysql_real_escape_string', $_GET);
//        $_POST = array_map('mysql_real_escape_string', $_POST);
//        $_REQUEST = array_map('mysql_real_escape_string', $_REQUEST);
    }

}

sqlinjection();
?>