<?php

/**
 * Plataforma kenGoo
 * @name kengoo
 * @version 0.9b
 * @author Heron Santos
 * @copyright www.heronsantos.com
 */
if (isset($_SERVER["REMOTE_ADDR"])) {

    if (!defined("uniqueid")) {
        define("uniqueid", uniqid());
    }

    session_start();
    require_once 'kernel/__autoload.php';
    require_once 'kernel/error.php';
    require_once 'kernel/println.php';
    require_once 'kernel/kernel.php';
    kernel::boot();
}
?>