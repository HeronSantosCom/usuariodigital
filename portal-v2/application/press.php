<?php

class press {

    public function __construct() {
        kernel::set("title", "Usuário Digital");
        kernel::extract($_GET);
    }

    public function content($content = "tag") {
        if (kernel::get("content")) {
            $content = kernel::get("content");
        }
        kernel::set("slider", source::get("index-slider.xhtml"));
        kernel::set("menu", source::get("index-menu.xhtml"));
        kernel::set("popular", source::get("index-popular.xhtml"));
        kernel::set("sites", source::get("index-sites.xhtml"));
        kernel::set("content", source::get("content-{$content}.xhtml"));
    }

}

?>