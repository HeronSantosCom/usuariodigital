<?php

class admin {

    public function __construct() {
        kernel::set("title", "Usuário Digital");
        kernel::extract($_GET);
    }

    public function login() {
        if (!kernel::get("usuario", "usuario")) {
            kernel::extract($_POST, "login");
            if (isset($_COOKIE["username"])) {
                if (strlen($_COOKIE["username"]) > 0) {
                    kernel::extract($_COOKIE, "login");
                }
            }
            if (kernel::get("login_username")) {
                $usuarios = new mysqlsearch();
                $usuarios->table("usuarios_view");
                $usuarios->column("*");
                $usuarios->match("usuario", kernel::get("login_username"));
                $usuarios->match("senha", md5(kernel::get("login_password")), "AND");
                $usuarios = $usuarios->go();
                if (isset($usuarios[0])) {
                    if (kernel::get("login_remember")) {
                        setcookie("username", kernel::get("login_username"), time() + 60 * 60 * 24 * 100, "/");
                        setcookie("password", kernel::get("login_password"), time() + 60 * 60 * 24 * 100, "/");
                    }
                    kernel::extract($usuarios[0], false, "usuario");
                    header("Location: painel.html");
                    exit;
                } else {
                    kernel::set("mensagem", source::get("admin/index-erro.xhtml"));
                }
            }
            return false;
        }
        header("Location: painel.html");
        exit;
    }

    public function logoff() {
        if (kernel::get("usuario", "usuario")) {
            kernel::clear("usuario");
            if (isset($_COOKIE["username"])) {
                if (strlen($_COOKIE["username"]) > 0) {
                    setcookie("username", false, time() + 60 * 60 * 24 * 100, "/");
                    setcookie("password", false, time() + 60 * 60 * 24 * 100, "/");
                }
            }
        }
        header("Location: index.html");
        exit;
    }

    public function senha() {
        
    }

    public function content($content = "dashboard") {
        if (kernel::get("usuario", "usuario")) {
            if (kernel::get("content")) {
                $content = kernel::get("content");
            }
            kernel::set("menu", source::get("admin/painel-menu.xhtml"));
            kernel::set("content", source::get("admin/content-{$content}.xhtml"));
            return true;
        }
        header("Location: ../index.html");
        exit;
    }

}

?>