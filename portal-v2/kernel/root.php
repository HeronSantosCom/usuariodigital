<?php

if (!class_exists('root')) {

    /**
     * Detecta os caminhos e endereços do sistema.
     * @name kengoo/kernel/root
     * @author Heron Santos
     * @copyright www.heronsantos.com
     */
    class root {

        /**
         * Extrai os caminhos do sistema.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $file <p>Arquivo, padrão = __file__</p>
         * @return void
         */
        static function boot($file = __file__) {
            kernel::clear("root");
            //extrai o caminho da raiz
            $pathinfo = pathinfo($file);
            $dirname = str_replace("\\", "/", $pathinfo["dirname"]);
            $path = explode('/kernel', $dirname);
            kernel::set('path', $path[0], 'root');
            //extrai o diretorio da raiz
            $path = explode('/', kernel::get('path', 'root'));
            $dir = array_pop($path);
            kernel::set('dir', "/{$dir}", 'root');
            if (isset($_SERVER['REQUEST_URI'])) {
                //extrai a uri
                $dir = kernel::get('dir', 'root');
                if (preg_match("#" . str_replace(".", "\.", $dir) . "#", urldecode($_SERVER['REQUEST_URI']))) {
                    $uri = explode($dir, urldecode($_SERVER['REQUEST_URI']), 2);
                    $uri = $uri[0] . $dir;
                    if (substr($uri, 0, 1) != "/") {
                        $uri = "/{$uri}";
                    }
                    kernel::set('uri', $uri, 'root');
                }
                //extrai o host
                $url_path = "http://127.0.0.1";
                if (isset($_SERVER["HTTP_HOST"]) and strlen($_SERVER["HTTP_HOST"]) > 0) {
                    $prefix = "http://";
                    if (isset($_SERVER["HTTPS"])) {
                        $prefix = "https://";
                    }
                    $url_path = $prefix . $_SERVER["HTTP_HOST"];
                }
                kernel::set('host', $url_path . kernel::get('uri', 'root'), 'root');
            }
        }

        /**
         * Caminho físico do sistema.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $name <p>Sufixo, padrão = NULL</p>
         * @return string
         */
        static function path($filename = null) {
            if (substr($filename, (strlen($filename) - 1)) == "/") {
                $filename = substr($filename, 0, (strlen($filename) - 1));
            }
            return kernel::get("path", "root") . ($filename ? "/{$filename}" : null);
        }

        /**
         * Diretório virtual do sistema.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $name <p>Sufixo, padrão = NULL</p>
         * @return string
         */
        static function dir($filename = null) {
            if (substr($filename, (strlen($filename) - 1)) == "/") {
                $filename = substr($filename, 0, (strlen($filename) - 1));
            }
            return kernel::get("dir", "root") . ($filename ? "/{$filename}" : null);
        }

        /**
         * Caminho virtual do sistema.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $name <p>Sufixo, padrão = NULL</p>
         * @return string
         */
        static function host($filename = null) {
            if (substr($filename, (strlen($filename) - 1)) == "/") {
                $filename = substr($filename, 0, (strlen($filename) - 1));
            }
            return kernel::get("host", "root") . ($filename ? "/{$filename}" : null);
        }

        /**
         * Localização de Recursos Uniforme (URI ou URL) do sistema.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $name <p>Sufixo, padrão = NULL</p>
         * @return string
         */
        static function uri($filename = null) {
            if (substr($filename, 0, 1) == "/") {
                $filename = substr($filename, 1);
            }
            if (substr($filename, (strlen($filename) - 1)) == "/") {
                $filename = substr($filename, 0, (strlen($filename) - 1));
            }
            return kernel::get("uri", "root") . ($filename ? "/{$filename}" : "/");
        }

    }

}

root::boot();
?>