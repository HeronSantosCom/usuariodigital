<?php

class MySQLSave {

    private $instance = false;
    private $ignore = false;
    private $database = false;
    private $table = false;
    private $columns = false;
    private $where = false;
    private $rows = false;
    private $id = false;
    public $result = false;

    public function __construct($instance = false) {
        $this->instance = MySQL::GetInstance($instance);
        if (!$this->instance) {
            trigger_error("Nenhuma instância válida foi declarada ou conexão com o banco de dados não estabelecido!", E_USER_WARNING);
            return false;
        }
    }

    public function Dabatase($name) {
        $index = $this->Index("database");
        $this->database[$index] = MySQLFormat::Ereaser($name, "`");
        return $this;
    }

    public function Table($name, $databasekey = 1) {
        $database = (isset($this->database[$databasekey]) ? "{$this->database[$databasekey]}." : null);
        $table = MySQLFormat::Ereaser($name, "`");
        $index = $this->Index("table");
        $this->table[$index] = "{$database}{$table}";
        return $this;
    }

    public function Column($name, $value = null, $tablekey = false) {
        if (!MySQLFormat::IsFunction($value)) {
            $table = (isset($this->table[0]) ? "{$this->table[0]}." : null);
            if ($tablekey) {
                $table = (isset($this->table[$tablekey]) ? "{$this->table[$tablekey]}." : $table);
            }
            $column = MySQLFormat::Ereaser($name, "`");
            $value = (strlen($value) > 0 ? "\"" . mysql_real_escape_string($value, $this->instance->link) . "\"" : "NULL");
            $this->columns[] = $table . $column . " = " . $value;
            return $this;
        }
        trigger_error("Valor inserido não permitido!", E_USER_WARNING);
        return false;
    }

    public function Match($column, $value, $inverse = false, $tablekey = false) {
        if (!MySQLFormat::IsFunction($column)) {
            $table = (isset($this->table[0]) ? "{$this->table[0]}." : null);
            if ($tablekey) {
                $table = (isset($this->table[$tablekey]) ? "{$this->table[$tablekey]}." : $table);
            }
            $column = $table . MySQLFormat::Ereaser($column, "`", false);
        }
        if (!MySQLFormat::IsFunction($value)) {
            $value = (strlen($value) > 0 ? "\"" . mysql_real_escape_string($value, $this->instance->link) . "\"" : "NULL");
        }
        $inverse = ($inverse ? "<>" : "=");
        return $this->Where("{$column} {$inverse} {$value}");
    }

    public function In($column, $value, $inverse = false, $tablekey = false) {
        if (is_array($value)) {
            if (!MySQLFormat::IsFunction($column)) {
                $table = (isset($this->table[0]) ? "{$this->table[0]}." : null);
                if ($tablekey) {
                    $table = (isset($this->table[$tablekey]) ? "{$this->table[$tablekey]}." : $table);
                }
                $column = $table . MySQLFormat::Ereaser($column, "`", false);
            }
            foreach ($value as $key => $aux) {
                if (!MySQLFormat::IsFunction($aux)) {
                    $value[$key] = (strlen($aux) > 0 ? "\"" . mysql_real_escape_string($aux, $this->instance->link) . "\"" : "NULL");
                }
            }
            $value = join(", ", $value);
            $inverse = ($inverse ? "NOT IN" : "IN");
            return $this->Where("{$column} {$inverse} ({$value})");
        }
        trigger_error("É necessário a passagem do valor em array!", E_USER_WARNING);
        return false;
    }

    public function Like($column, $value, $inverse = false, $tablekey = false) {
        if (!MySQLFormat::IsFunction($column)) {
            $table = (isset($this->table[0]) ? "{$this->table[0]}." : null);
            if ($tablekey) {
                $table = (isset($this->table[$tablekey]) ? "{$this->table[$tablekey]}." : $table);
            }
            $column = $table . MySQLFormat::Ereaser($column, "`", false);
        }
        if (!MySQLFormat::IsFunction($value)) {
            $value = (strlen($value) > 0 ? "\"" . mysql_real_escape_string($value, $this->instance->link) . "\"" : "NULL");
        }
        $inverse = ($inverse ? "NOT LIKE" : "LIKE");
        return $this->Where("{$column} {$inverse} {$value}");
    }

    public function Where($conditions) {
        $index = $this->Index("where");
        $this->where[$index] = $conditions;
        return $this;
    }

    public function Go($ignore = false) {
        if ($ignore) {
            $ignore = "IGNORE";
        }
        if ($this->table) {
            $table = join(", ", $this->table);
            if ($this->columns) {
                $columns = join(", ", $this->columns);
                $query = "INSERT {$ignore} INTO {$table} SET {$columns}";
                if ($this->where) {
                    $where = join(" AND ", $this->where);
                    $query = "UPDATE {$ignore} {$table} SET {$columns} WHERE {$where}";
                }
                $this->result = $this->instance->Commit($query);
                if ($this->result["result"]) {
                    $this->rowns = $this->result["rows"];
                    $this->id = $this->result["id"];
                    return true;
                }
                return false;
            }
            trigger_error("Coluna/valores não definida!", E_USER_WARNING);
            return false;
        }
        trigger_error("Tabela não definida!", E_USER_WARNING);
        return false;
    }

    public function GetRows() {
        return $this->rowns;
    }

    public function GetID() {
        return $this->id;
    }

    protected function Index($object, $get = false) {
        if ($get) {
            if (isset($this->index[$object])) {
                return $this->index[$object];
            }
            return false;
        }
        return $this->index[$object] = (isset($this->index[$object]) ? $this->index[$object] + 1 : 1);
    }

}

?>
