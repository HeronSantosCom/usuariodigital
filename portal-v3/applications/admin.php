<?php

class admin extends control {

    public function __construct() {
        $this->extract($_GET);
    }

    public function login() {
        if (!isset($_SESSION["usuario"])) {
            $this->extract($_POST);
            if (isset($_COOKIE["username"])) {
                if (strlen($_COOKIE["username"]) > 0) {
                    $this->extract($_COOKIE);
                }
            }
            if ($this->username) {
                $usuarios = new mysqlsearch();
                $usuarios->table("usuarios_view");
                $usuarios->column("*");
                $usuarios->match("usuario", $this->username);
                $usuarios->match("senha", md5($this->password), "AND");
                $usuarios = $usuarios->go();
                if (isset($usuarios[0])) {
                    if ($this->remember) {
                        setcookie("username", $this->username, time() + 60 * 60 * 24 * 100, "/");
                        setcookie("password", $this->password, time() + 60 * 60 * 24 * 100, "/");
                    }
                    $_SESSION["usuario"] = $usuarios[0];
                    $this->redirect("painel.html");
                }
                $this->mensagem = $this->html("index-erro.xhtml");
            }
            return false;
        }
        $this->redirect("painel.html");
    }

    public function logoff() {
        if (isset($_SESSION["usuario"])) {
            unset($_SESSION["usuario"]);
            if (isset($_COOKIE["username"])) {
                if (strlen($_COOKIE["username"]) > 0) {
                    setcookie("username", false, time() + 60 * 60 * 24 * 100, "/");
                    setcookie("password", false, time() + 60 * 60 * 24 * 100, "/");
                }
            }
        }
        $this->redirect("index.html");
    }

    public function senha() {
        
    }

    public function content($content = "dashboard") {
        if (isset($_SESSION["usuario"])) {
            if ($this->content) {
                $content = $this->content;
            }
            $this->menu = $this->html("painel-menu.xhtml");
            $this->content = $this->html("content/{$content}.xhtml");
            return true;
        }
        $this->redirect("index.html");
    }

    public function logged() {
        if (isset($_SESSION["usuario"])) {
            return true;
        }
        $this->redirect("index.html");
    }

}

?>