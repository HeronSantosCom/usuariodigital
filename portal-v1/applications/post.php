<?php

class Post {

    private $ids, $search, $options;
    private $page = 1;

    public function __construct() {
        Index::MySQL();
        System::Set("Title", "Postagens");
        if (isset($_GET["options"])) {
            $this->options = $_GET["options"];
        }
    }

    public function SetId($id = false) {
        return $this->ids = ($id ? $id : Permalink::Get(1));
    }

    public function SetPage($page = false) {
        return $this->page = ($page ? $page : (Permalink::Get(1) ? Permalink::Get(1) : 1));
    }

    public function SetSearch($search = false) {
        if (isset($_POST["search"])) {
            $this->search = $_POST["search"];
            $this->page = 1;
            System::Set("Redirect", "admin/internal/dashboard/post/1/{$this->search}", "Admin");
            return true;
        }
        return $this->search = ($search ? $search : Permalink::Get(2));
    }

    public function Save() {
        System::Set("status", "1", "Form");
        System::Set("ips_account_id", System::Get("id", "Authenticate"), "Form");
        if (isset($_POST["ips_confirm"])) {
            $this->Clean();
            System::Extract($_POST, false, false, "Form");
            $saved = (!$this->ids ? $this->Insert() : $this->Update($this->ids));
            if ($saved) {
                System::Redirect(Core::Remote(System::Get("Redirect", "Admin")));
                Admin::Alert("Registro salvado!");
                return true;
            }
            Admin::Alert("Problemas ao efetuar operação!");
        }
        return false;
    }

    public function Delete() {
        if ($this->ids) {
            $delete = new MySQLDelete();
            $delete->Table("ips_post");
            $delete->In("id", explode(",", $this->ids));
            if ($delete->Go()) {
                System::Redirect(Core::Remote(System::Get("Redirect", "Admin")));
                Admin::Alert("Registro(s) removido(s)!");
                return true;
            }
        }
        Admin::Alert("Problemas ao efetuar operação!");
        return false;
    }

    public function Open() {
        if ($this->ids) {
            $search = new MySQLSearch();
            $search->Table("ips_post");
            $search->Column("*");
            $search->Match("id", $this->ids);
            $search->Limit(1);
            $search = $search->Go();
            if ($search) {
                System::Extract($search[0], null, null, "Form");
                return true;
            }
            Admin::Alert("Registro não encontrado!");
        }
        return false;
    }

    public function Search() {
        $search = new MySQLSearch();
        $search->Table("ips_post");
        $search->Column("id");
        $search->Column("title");
        $search->Column("status");
        $search->Column("DATE_FORMAT(`insert`, '%d/%m/%Y %H:%i')", false, "insert_date");
        if ($this->search) {
            $search->Like("title", "%{$this->search}%");
            $search->Like("metatag", "%{$this->search}%", "OR");
            $search->Like("DATE_FORMAT(`insert`, '%d/%m/%Y %H:%i')", "%{$this->search}%", "OR");
            System::Set("Search Text", $this->search);
        }
        $search->Order(2);
        $search->limit = $this->Pagination($search->where);
        $search = $search->Go();
        if ($search) {
            return System::Set("Search", $search);
        }
        return false;
    }

    public function Options() {
        $search = new MySQLSearch();
        $search->Table("ips_post");
        $search->Column("id");
        $search->Column("title");
        $search->Order(2);
        if ($this->options) {
            $search->Like("title", "%{$this->options}%");
        }
        System::Set("Options Post", $search->Go());
    }

    public function OptionsGallery() {
        $search = new MySQLSearch();
        $search->Table("ips_gallery_has_ips_post");
        $search->Join("ips_gallery", array("ips_gallery_id", "=", "id"), "LEFT");
        $search->Column("id", 2);
        $search->Column("title", 2);
        $search->Match("ips_post_id", $this->ids, false, false);
        System::Set("Options Associate", $search->Go());
    }

    private function Pagination($where) {
        $total = new MySQLSearch();
        $total->Table("ips_post");
        $total->Column("COUNT(id)", false, "total");
        $total->where = $where;
        $pagination = System::Pages($total, $this->page, admin_limit);
        if ($pagination) {
            System::Set("Search Pagination", $pagination[1]);
            return $pagination[0];
        }
        return false;
    }

    private function Clean() {
        $clean = new MySQLShow();
        $clean->Table("ips_post");
        $fields = $clean->Columns();
        if ($fields) {
            foreach ($fields as $field) {
                System::Set($field["Field"], false, "Form");
            }
        }
    }

    private function Insert() {
        $metatag = System::KeyWords(System::Get("text", "Form"));
        if (is_array($metatag)) {
            $metatag = join(", ", array_keys($metatag));
        }
        $save = new MySQLSave();
        $save->Table("ips_post");
        $save->Column("title", System::Get("title", "Form"));
        $save->Column("text", System::Get("text", "Form"));
        $save->Column("metatag", $metatag);
        $save->Column("status", System::Get("status", "Form"));
        $save->Column("insert", date("Y-m-d H:i:s"));
        $save->Column("ips_account_id", System::Get("ips_account_id", "Form"));
        $save->Column("ips_category_id", System::Get("ips_category_id", "Form"));
        if ($save->Go()) {
            return $this->GalleryAssociate($save->GetID(), System::Get("ips_gallery", "Form"));
        }
        return false;
    }

    private function Update($id) {
        $metatag = System::KeyWords(System::Get("text", "Form"));
        if (is_array($metatag)) {
            $metatag = join(", ", array_keys($metatag));
        }
        $save = new MySQLSave();
        $save->Table("ips_post");
        $save->Column("title", System::Get("title", "Form"));
        $save->Column("text", System::Get("text", "Form"));
        $save->Column("metatag", $metatag);
        $save->Column("status", System::Get("status", "Form"));
        $save->Column("ips_account_id", System::Get("ips_account_id", "Form"));
        $save->Column("ips_category_id", System::Get("ips_category_id", "Form"));
        $save->Match("id", $id);
        if ($save->Go()) {
            $this->GalleryDesassociate($id);
            return $this->GalleryAssociate($id, System::Get("ips_gallery", "Form"));
        }
        return false;
    }

    public function GalleryAssociate($ips_post_id, $ips_gallery) {
        $ips_gallery = explode(",", $ips_gallery);
        if (is_array($ips_gallery)) {
            foreach ($ips_gallery as $ips_gallery_id) {
                $save = new MySQLSave();
                $save->Table("ips_gallery_has_ips_post");
                $save->Column("ips_gallery_id", $ips_gallery_id);
                $save->Column("ips_post_id", $ips_post_id);
                $save->Go();
            }
        }
        return true;
    }

    public function GalleryDesassociate($ips_post_id) {
        $delete = new MySQLDelete();
        $delete->Table("ips_gallery_has_ips_post");
        $delete->Match("ips_post_id", $ips_post_id);
        $delete->Go();
        return true;
    }

}

?>