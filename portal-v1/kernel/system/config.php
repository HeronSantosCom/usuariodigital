<?php

if (!function_exists("config")) {

    /**
     * Carrega as configurações a partir de um determinado Objeto (XML) e as define como constante
     * @param object $object
     * @param string $name
     */
    function config($object, $name = false) {
        if ($object) {
            foreach ($object as $key => $value) {
                $new = ($name ? $name . "_" . $key : $key);
                $string = trim((string) $value);
                if (strlen($string) > 0) {
                    if (!defined($new)) {
                        switch ($string) {
                            case "true":
                                $string = true;
                                break;
                            case "false":
                                $string = false;
                                break;
                            case "null":
                                $string = null;
                                break;
                        }
                        define($new, $string);
                    }
                } else {
                    if (!is_null($key)) {
                        config($value, $new);
                    }
                }
            }
        }
    }

}
?>
