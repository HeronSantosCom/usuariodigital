<?php

class MySQLDelete {

    private $instance = false;
    private $database = false;
    private $table = false;
    private $columns = false;
    private $where = false;
    private $rows = false;
    private $id = false;
    public $result = false;

    public function __construct($instance = false) {
        $this->instance = MySQL::GetInstance($instance);
        if (!$this->instance) {
            trigger_error("Nenhuma instância válida foi declarada ou conexão com o banco de dados não estabelecido!", E_USER_WARNING);
            return false;
        }
    }

    public function Dabatase($name) {
        $index = $this->Index("database");
        $this->database[$index] = MySQLFormat::Ereaser($name, "`");
        return $this;
    }

    public function Table($name, $databasekey = 1) {
        $database = (isset($this->database[$databasekey]) ? "{$this->database[$databasekey]}." : null);
        $table = MySQLFormat::Ereaser($name, "`");
        $index = $this->Index("table");
        $this->table[$index] = "{$database}{$table}";
        return $this;
    }

    public function Match($column, $value, $inverse = false, $tablekey = 1) {
        if (!MySQLFormat::IsFunction($column)) {
            $table = (isset($this->table[0]) ? "{$this->table[0]}." : null);
            if ($tablekey) {
                $table = (isset($this->table[$tablekey]) ? "{$this->table[$tablekey]}." : $table);
            }
            $column = $table . MySQLFormat::Ereaser($column, "`", false);
        }
        if (!MySQLFormat::IsFunction($value)) {
            $value = (strlen($value) > 0 ? "\"" . mysql_real_escape_string($value, $this->instance->link) . "\"" : "NULL");
        }
        $inverse = ($inverse ? "<>" : "=");
        return $this->Where("{$column} {$inverse} {$value}");
    }

    public function In($column, $value, $inverse = false, $tablekey = 1) {
        if (is_array($value)) {
            if (!MySQLFormat::IsFunction($column)) {
                $table = (isset($this->table[0]) ? "{$this->table[0]}." : null);
                if ($tablekey) {
                    $table = (isset($this->table[$tablekey]) ? "{$this->table[$tablekey]}." : $table);
                }
                $column = $table . MySQLFormat::Ereaser($column, "`", false);
            }
            foreach ($value as $key => $aux) {
                if (!MySQLFormat::IsFunction($aux)) {
                    $value[$key] = (strlen($aux) > 0 ? "\"" . mysql_real_escape_string($aux, $this->instance->link) . "\"" : "NULL");
                }
            }
            $value = join(", ", $value);
            $inverse = ($inverse ? "NOT IN" : "IN");
            return $this->Where("{$column} {$inverse} ({$value})");
        }
        trigger_error("É necessário a passagem do valor em array!", E_USER_WARNING);
        return false;
    }

    public function Like($column, $value, $inverse = false, $tablekey = 1) {
        if (!MySQLFormat::IsFunction($column)) {
            $table = (isset($this->table[0]) ? "{$this->table[0]}." : null);
            if ($tablekey) {
                $table = (isset($this->table[$tablekey]) ? "{$this->table[$tablekey]}." : $table);
            }
            $column = $table . MySQLFormat::Ereaser($column, "`", false);
        }
        if (!MySQLFormat::IsFunction($value)) {
            $value = (strlen($value) > 0 ? "\"" . mysql_real_escape_string($value, $this->instance->link) . "\"" : "NULL");
        }
        $inverse = ($inverse ? "NOT LIKE" : "LIKE");
        return $this->Where("{$column} {$inverse} {$value}");
    }

    public function Where($conditions) {
        $index = $this->Index("where");
        $this->where[$index] = $conditions;
        return $this;
    }

    public function Go() {
        $where = null;
        if ($this->table) {
            $table = join(", ", $this->table);
            if ($this->where) {
                $where = " WHERE " . join(" AND ", $this->where);
            }
            $query = "DELETE FROM {$table}{$where}";
            $this->result = $this->instance->Commit($query);
            if ($this->result["result"]) {
                $this->rowns = $this->result["rows"];
                if ($this->result["rows"] > 0) {
                    return true;
                }
            }
            return false;
        }
        trigger_error("Tabela não definida!", E_USER_WARNING);
        return false;
    }

    public function GetRows() {
        return $this->rowns;
    }

    protected function Index($object, $get = false) {
        if ($get) {
            if (isset($this->index[$object])) {
                return $this->index[$object];
            }
            return false;
        }
        return $this->index[$object] = (isset($this->index[$object]) ? $this->index[$object] + 1 : 1);
    }

}

?>
