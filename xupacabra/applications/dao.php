<?php

class dao {

    public static function abrir() {
        $db = new mysqlsearch();
        $db->table("wp_posts");
        $db->column("*");
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0]);
        }
        return false;
    }

    private static function hook($row) {
        return $row;
    }

}
