<?php

class Admin {

    public function SetRedirect() {
        System::Set("Redirect", Core::Opened(), "Admin");
    }

    static function Alert($message) {
        print '<script type="text/javascript">alert("' . addslashes($message) . '");</script>';
        return false;
    }

    static function Go($link, $target = '_parent') {
        print '<script type="text/javascript">doOpen("' . $link . '", "' . $target . '");</script>';
        exit;
    }

}

?>