<?php

//TODO: dividir os indices em array, contendo os itens e alias
class MySQLSearch {

    private $instance = false;
    private $rows = false;
    private $index = false;
    public $database = false;
    public $table = false;
    public $columns = false;
    public $where = false;
    public $group = false;
    public $having = false;
    public $order = false;
    public $limit = false;
    public $result = false;

    public function __construct($instance = false) {
        $this->instance = MySQL::GetInstance($instance);
        if (!$this->instance) {
            trigger_error("Nenhuma instância válida foi declarada ou conexão com o banco de dados não estabelecido!", E_USER_WARNING);
            return false;
        }
    }

    public function Dabatase($name) {
        $index = $this->Index("database");
        $this->database[$index] = MySQLFormat::Ereaser($name, "`");
        return $this;
    }

    public function Table($name, $databasekey = 1, $alias = false) {
        $database = (isset($this->database[$databasekey]) ? "{$this->database[$databasekey]}." : null);
        $table = MySQLFormat::Ereaser($name, "`");
        $index = $this->Index("table");
        $alias = ($alias ? $alias : MySQLFormat::Ereaser("t{$index}", "`"));
        $this->table[$index] = array(false, "{$database}{$table} {$alias}", $alias);
        return $this;
    }

    public function Join($name, $condition = false, $factor = false, $databasekey = 1, $alias = false) {
        $index_condition = $this->Index("table", true);
        $database = (isset($this->database[$databasekey]) ? "{$this->database[$databasekey]}." : null);
        $table = MySQLFormat::Ereaser($name, "`");
        $index = $this->Index("table");
        $alias = ($alias ? $alias : MySQLFormat::Ereaser("t{$index}", "`"));

        switch ($factor) {
            case "LEFT":
                $join = "LEFT JOIN";
                break;
            case "RIGHT":
                $join = "RIGHT JOIN";
                break;
            case "INNER":
                $join = "INNER JOIN";
                break;
            default:
                if (!$factor) {
                    $join = "JOIN";
                } else {
                    trigger_error("Fator para fazer o JOIN na sua consulta não é permitido!", E_USER_WARNING);
                    return false;
                }
                break;
        }

        $on = null;
        if ($condition) {
            if (is_array($condition)) {
                if (!isset($this->table[$index_condition])) {
                    trigger_error("Tabela para comparação não declarada anteriormente!", E_USER_WARNING);
                    return false;
                }
                $column_condition_1 = MySQLFormat::Ereaser($condition[0], "`");
                $column_condition_2 = (isset($condition[2]) ? MySQLFormat::Ereaser($condition[2], "`") : $column_condition_1);
                $table_condition_1 = $this->table[(isset($condition[3]) ? $condition[3] : $index_condition)][2] . ".{$column_condition_1}";
                $table_condition_2 = (isset($condition[4]) ? $this->table[$condition[4]][2] : $alias) . ".{$column_condition_2}";
                //$table_condition_2 = "{$alias}.{$column_condition_2}";
                $operator = (isset($condition[1]) ? trim($condition[1]) : "=");
                $condition = "{$table_condition_1} {$operator} {$table_condition_2}";
            }
            $on = " ON ({$condition})";
        }

        $this->table[$index] = array(true, "{$join} {$database}{$table} {$alias}{$on}", $alias);
        return $this;
    }

    public function Column($name, $tablekey = 1, $alias = false) {
        $table = null;
        if (!MySQLFormat::IsFunction($alias)) {
            $index = $this->Index("column");
            if (!MySQLFormat::IsFunction($name)) {
                if (!isset($this->table[$tablekey])) {
                    trigger_error("Tabela solicitada não declarada!", E_USER_WARNING);
                    return false;
                }
                $column = MySQLFormat::Ereaser($name);
                if ($name != "*") {
                    //$alias = $column;
                    $table = "{$this->table[$tablekey][2]}.";
                    $column = $table . MySQLFormat::Ereaser($column, "`", false);
                }
            } else {
                if ($tablekey) {
                    trigger_error("Não é possível determinar tabela em uma função!", E_USER_WARNING);
                    return false;
                } else {
                    if (!$alias) {
                        trigger_error("Apelido para a função não determinado!", E_USER_WARNING);
                        return false;
                    }
                }
                $column = $name;
                $alias = MySQLFormat::Ereaser($alias);
            }
            $this->columns[$index] = array($column . ($alias ? " AS '{$alias}'" : null), $alias);
            return $this;
        }
        trigger_error("Apelido inserido não permitido!", E_USER_WARNING);
        return false;
    }

    public function Match($column, $value, $glue = false, $inverse = false, $tablekey = 1) {
        if (!MySQLFormat::IsFunction($column)) {
            if (!isset($this->table[$tablekey])) {
                trigger_error("Tabela solicitada não declarada!", E_USER_WARNING);
                return false;
            }
            $column = "{$this->table[$tablekey][2]}." . MySQLFormat::Ereaser($column, "`", false);
        }
        if (!MySQLFormat::IsFunction($value)) {
            $value = (strlen($value) > 0 ? "\"" . mysql_real_escape_string($value, $this->instance->link) . "\"" : "NULL");
        }
        $inverse = ($inverse ? "<>" : "=");
        return $this->Where(($glue ? "{$glue} " : null) . "{$column} {$inverse} {$value}");
    }

    public function In($column, $value, $glue = false, $inverse = false, $tablekey = 1) {
        if (is_array($value)) {
            if (!MySQLFormat::IsFunction($column)) {
                if (!isset($this->table[$tablekey])) {
                    trigger_error("Tabela solicitada não declarada!", E_USER_WARNING);
                    return false;
                }
                $column = "{$this->table[$tablekey][2]}." . MySQLFormat::Ereaser($column, "`", false);
            }
            foreach ($value as $key => $aux) {
                if (!MySQLFormat::IsFunction($aux)) {
                    $value[$key] = (strlen($aux) > 0 ? "\"" . mysql_real_escape_string($aux, $this->instance->link) . "\"" : "NULL");
                }
            }
            $value = join(", ", $value);
            $inverse = ($inverse ? "NOT IN" : "IN");
            return $this->Where(($glue ? "{$glue} " : null) . "{$column} {$inverse} ({$value})");
        }
        trigger_error("É necessário a passagem do valor em array!", E_USER_WARNING);
        return false;
    }

    public function Like($column, $value, $glue = false, $inverse = false, $tablekey = 1) {
        if (!MySQLFormat::IsFunction($column)) {
            if (!isset($this->table[$tablekey])) {
                trigger_error("Tabela solicitada não declarada!", E_USER_WARNING);
                return false;
            }
            $column = "{$this->table[$tablekey][2]}." . MySQLFormat::Ereaser($column, "`", false);
        }
        if (!MySQLFormat::IsFunction($value)) {
            $value = (strlen($value) > 0 ? "\"" . mysql_real_escape_string($value, $this->instance->link) . "\"" : "NULL");
        }
        $inverse = ($inverse ? "NOT LIKE" : "LIKE");
        return $this->Where(($glue ? "{$glue} " : null) . "{$column} {$inverse} {$value}");
    }

    public function Where($conditions) {
        $index = $this->Index("where");
        $this->where[$index] = $conditions;
        return $this;
    }

    public function Group($columnkey) {
        if (!isset($this->columns[$columnkey])) {
            trigger_error("Coluna solicitada não declarada!", E_USER_WARNING);
            return false;
        }
        $index = $this->Index("group");
        $this->group[$index] = ($this->columns[$columnkey][1] ? MySQLFormat::Ereaser($this->columns[$columnkey][1], "`") : $this->columns[$columnkey][0]);
        return $this;
    }

    public function Having($columnkey, $value, $operator = "=") {
        if (!MySQLFormat::IsFunction($columnkey)) {
            if (!isset($this->columns[$columnkey])) {
                trigger_error("Coluna solicitada não declarada!", E_USER_WARNING);
                return false;
            }
            $column = $this->columns[$columnkey][1];
        }
        if (!MySQLFormat::IsFunction($value)) {
            $value = (strlen($value) > 0 ? "\"" . mysql_real_escape_string($value, $this->instance->link) . "\"" : "NULL");
        }
        $index = $this->Index("having");
        $this->having[$index] = "{$column} {$operator} {$value}";
        return $this;
    }

    public function Order($columnkey, $order = "ASC") {
        if (!isset($this->columns[$columnkey])) {
            trigger_error("Coluna solicitada não declarada!", E_USER_WARNING);
            return false;
        }
        $index = $this->Index("order");
        $this->order[$index] = ($this->columns[$columnkey][1] ? MySQLFormat::Ereaser($this->columns[$columnkey][1], "`") : $this->columns[$columnkey][0]) . strtoupper(" {$order}");
        return $this;
    }

    public function Limit($limit, $length = false) {
        $this->limit = $limit . ($length ? ", {$length}" : null);
        return $this;
    }

    public function Show() {
        $table = null;
        if ($this->table) {
            foreach ($this->table as $value) {
                $table .= ( $value[0] ? " " : (is_null($table) ? null : ", ")) . $value[1];
            }
            $query = "SHOW /*!32332 FULL */ COLUMNS FROM {$table}";
            $this->result = $this->instance->Commit($query);
            if ($this->result["result"]) {
                $this->rows = $this->result["rows"];
                $this->id = $this->result["id"];
                $this->search = false;
                if (@mysql_num_rows($this->result["result"]) > 0) {
                    while ($line = mysql_fetch_array($this->result["result"], MYSQL_ASSOC)) {
                        $this->search[] = $line;
                    }
                }
                return $this->search;
            }
            return false;
        }
        trigger_error("Tabela não declarada!", E_USER_WARNING);
        return false;
    }

    public function Go() {
        $table = $columns = $where = $group = $having = $order = $limit = null;
        if ($this->table) {
            foreach ($this->table as $value) {
                $table .= ( $value[0] ? " " : (is_null($table) ? null : ", ")) . $value[1];
            }
            if ($this->columns) {
                foreach ($this->columns as $value) {
                    $columns .= ( is_null($columns) ? null : ", ") . $value[0];
                }
                if ($this->where) {
                    $where = " WHERE " . join(" ", $this->where);
                }
                if ($this->group) {
                    $group = " GROUP BY " . join(", ", $this->group);
                }
                if ($this->having) {
                    $having = " HAVING " . join(" AND ", $this->having);
                }
                if ($this->order) {
                    $order = " ORDER BY " . join(", ", $this->order);
                }
                if ($this->limit) {
                    $limit = " LIMIT " . $this->limit;
                }
                $query = "SELECT {$columns} FROM {$table}{$where}{$group}{$having}{$order}{$limit}";
                $this->result = $this->instance->Commit($query);
                if ($this->result["result"]) {
                    $this->rows = $this->result["rows"];
                    $this->id = $this->result["id"];
                    $this->search = false;
                    if (@mysql_num_rows($this->result["result"]) > 0) {
                        while ($line = mysql_fetch_array($this->result["result"], MYSQL_ASSOC)) {
                            $this->search[] = $line;
                        }
                    }
                    return $this->search;
                }
                return false;
            }
            trigger_error("Coluna não declarada!", E_USER_WARNING);
            return false;
        }
        trigger_error("Tabela não declarada!", E_USER_WARNING);
        return false;
    }

    public function GetRows() {
        return $this->rows;
    }

    protected function Index($object, $get = false) {
        if ($get) {
            if (isset($this->index[$object])) {
                return $this->index[$object];
            }
            return false;
        }
        return $this->index[$object] = (isset($this->index[$object]) ? $this->index[$object] + 1 : 1);
    }

}

?>
