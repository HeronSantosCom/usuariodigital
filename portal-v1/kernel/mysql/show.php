<?php

//TODO: dividir os indices em array, contendo os itens e alias
class MySQLShow {

    private $instance = false;
    private $database = false;
    private $table = false;
    private $index = false;
    public $result = false;

    public function __construct($instance = false) {
        $this->instance = MySQL::GetInstance($instance);
        if (!$this->instance) {
            trigger_error("Nenhuma instância válida foi declarada ou conexão com o banco de dados não estabelecido!", E_USER_WARNING);
            return false;
        }
    }

    public function Dabatase($name) {
        $index = $this->Index("database");
        $this->database[$index] = MySQLFormat::Ereaser($name, "`");
        return $this;
    }

    public function Table($name, $databasekey = 1, $alias = false) {
        $database = (isset($this->database[$databasekey]) ? "{$this->database[$databasekey]}." : null);
        $table = MySQLFormat::Ereaser($name, "`");
        $index = $this->Index("table");
        $this->table[$index] = array(false, "{$database}{$table}");
        return $this;
    }

    public function Columns() {
        if ($this->table) {
            $this->search = false;
            foreach ($this->table as $value) {
                $query = "SHOW /*!32332 FULL */ COLUMNS FROM {$value[1]}";
                $this->result = $this->instance->Commit($query);
                if ($this->result["result"]) {
                    if (@mysql_num_rows($this->result["result"]) > 0) {
                        while ($line = mysql_fetch_array($this->result["result"], MYSQL_ASSOC)) {
                            $this->search[] = $line;
                        }
                    }
                }
            }
            return $this->search;
        }
        trigger_error("Tabela não declarada!", E_USER_WARNING);
        return false;
    }

    protected function Index($object, $get = false) {
        if ($get) {
            if (isset($this->index[$object])) {
                return $this->index[$object];
            }
            return false;
        }
        return $this->index[$object] = (isset($this->index[$object]) ? $this->index[$object] + 1 : 1);
    }

}

?>
