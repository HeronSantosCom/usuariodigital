<?php

if (!class_exists('kernel')) {

    /**
     * Responsável pelo gerenciamento do Sistema.
     * @name kengoo/kernel
     * @author Heron Santos
     * @copyright www.heronsantos.com
     */
    class kernel {

        /**
         * Certidão de execução unica do sistema.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @return string <p>Código de execução único</p>
         */
        static private function cert() {
            return md5(session_id() . (isset($_SERVER["HTTP_HOST"]) ? $_SERVER["HTTP_HOST"] : null));
        }

        /**
         * Resgata váriavel.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $name <p>Nome da variável</p>
         * @param string $parent <p>Alocação, padrão = kernel</p>
         * @return mixed <p>Conteúdo da variável, caso não exista retorna FALSE</p>
         */
        static function get($name, $parent = 'kernel') {
            $parent = strtolower($parent);
            if (isset($_SESSION[self::cert()][$parent][$name])) {
                return $_SESSION[self::cert()][$parent][$name];
            }
            return false;
        }

        /**
         * Declara váriavel.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $name <p>Nome da variável</p>
         * @param string $value <p>Valor a ser atribuido, caso FALSE destroi a variável do sistema, padrão = FALSE</p>
         * @param string $parent <p>Alocação, padrão = kernel</p>
         * @return mixed <p>Conteúdo atribuido, caso não exista retorna FALSE</p>
         */
        static function set($name, $value = false, $parent = 'kernel') {
            $parent = strtolower($parent);
            $_SESSION[self::cert()][$parent][$name] = $value;
            if (!$value) {
                unset($_SESSION[self::cert()][$parent][$name]);
            }
            return $value;
        }

        /**
         * Destroi váriavel.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $parent <p>Alocação, padrão = kernel</p>
         * @return void
         */
        static function clear($parent = 'kernel') {
            $parent = strtolower($parent);
            if (isset($_SESSION[self::cert()][$parent])) {
                unset($_SESSION[self::cert()][$parent]);
            }
        }

        /**
         * Extrai um array(multidimensional) para variável.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $array <p>Array (multidimensional) a ser extraido</p>
         * @param string $prefix <p>Prefixo a ser inserido na variável, padrão = FALSE</p>
         * @param string $parent <p>Alocação, padrão = kernel</p>
         * @return mixed <p>Valor extraido, caso contrário retorna FALSE</p>
         */
        static function extract($array, $prefix = false, $parent = 'kernel') {
            if (is_array($array)) {
                foreach ($array as $key => $value) {
                    if (is_array($value)) {
                        return self::extract($value, ($prefix ? "{$prefix}_{$key}" : $key), $parent);
                    }
                    self::set(($prefix ? "{$prefix}_{$key}" : $key), $value, $parent);
                }
            }
            return false;
        }

        /**
         * Inicia o sistema.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @return void
         */
        static function boot() {
            if (file_exists(root::path("kernel/configuration.xml"))) {
                $object = @simplexml_load_file(root::path("kernel/configuration.xml"));
                if (is_object($object)) {
                    self::config($object);
                }
            }
            //ini_set('error_reporting', E_ALL);
            //ini_set("display_errors", false);
            set_error_handler('error');
            register_shutdown_function('error');
            setlocale(LC_ALL, system_timezone_locale);
            date_default_timezone_set(system_timezone_unix);
            self::clear();
            try {
                echo source::get();
            } catch (Exception $exc) {
                if (system_logger_error) {
                    self::log($exc->getMessage(), "error.log"); // getTraceAsString()
                    if (system_logger_exception) {
                        self::log($exc->getTraceAsString(), "exception.log");
                    }
                }
                @session_unset();
                @session_destroy();
                exit(0);
            }
            self::trace();
            exit(1);
        }

        /**
         * Gera o rastreamento de execução do sistema.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @return void
         */
        static function trace() {
            if (system_logger_trace) {
                $trace = @debug_backtrace();
                if (is_array($trace)) {
                    array_pop($trace);
                    $trace = array_reverse($trace);
                    $x = 0;
                    foreach ($trace as $key => $line) {
                        if (isset($line["file"]) and !preg_match("#kernel/#", $line["file"])) {
                            $new[] = "{$x} {$line['file']}" . (isset($line["line"]) ? " line {$line["line"]}" : null);
                            $x++;
                        }
                    }
                    if (isset($new)) {
                        self::log(join("\n", $new), "trace.log");
                    }
                }
            }
        }

        /**
         * Gerar uma mensagem de evento do sistema.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @return void
         */
        static function log($message, $filename = "system.log") {
            if (!is_array($message)) {
                $message = str_replace("\r\n", "\n", $message);
                $message = str_replace("\n\r", "\n", $message);
                $message = str_replace("\n\n", "\n", $message);
                $message = explode("\n", $message);
            }
            $date = date('Y-m-d H:i:s');
            if (isset($_SERVER["REMOTE_ADDR"])) {
                $host = $_SERVER["REMOTE_ADDR"];
                $pid = getmypid();
            } else {
                $host = exec('whoami');
                $pid = getmypid();
            }
            foreach ($message as $line) {
                if (strlen(trim($line)) > 0) {
                    $print[] = "[{$date}] [{$host}] [{$pid}] {$line}";
                }
            }
            if (isset($print)) {
                $print[] = "";
                $date = date("Ymd");
                file_put_contents(root::path("logs/{$date}-{$host}-{$filename}"), join("\n", $print), FILE_APPEND);
            }
        }

        /**
         * Extrai as configurações de um Objeto XML definindo-os como constantes.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $object <p>Objeto XML a ser extraido</p>
         * @param string $name <p>Nome a ser definido, padrão = FALSE</p>
         * @return void
         */
        static private function config($object, $name = false) {
            if ($object) {
                foreach ($object as $key => $value) {
                    $new = ($name ? $name . "_" . $key : $key);
                    $string = trim((string) $value);
                    if (strlen($string) > 0) {
                        if (!defined($new)) {
                            switch ($string) {
                                case "true":
                                    $string = true;
                                    break;
                                case "false":
                                    $string = false;
                                    break;
                                case "null":
                                    $string = null;
                                    break;
                            }
                            define($new, $string);
                        }
                    } else {
                        if (!is_null($key)) {
                            self::config($value, $new);
                        }
                    }
                }
            }
        }

        /**
         * Carrega um documento ou uma página de Internet.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $url <p>Endereço a ser carregado</p>
         * @param string $method [ curl | fopen ] <p>Método de carregamento, padrão = curl</p>
         * @return string <p>Conteúdo do documento/página, caso não exista retorna FALSE</p>
         */
        static function open($url, $method = "curl") {
            $contents = false;
            switch (strtolower($method)) {
                case "fopen":
                    ini_set("user_agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");
                    $pointer = @fopen($url, "r");
                    if ($pointer) {
                        $contents = null;
                        while (!feof($pointer)) {
                            $contents .= @ fgets($pointer, 4096);
                        }
                        fclose($pointer);
                    }
                    break;
                case "curl":
                default:
                    $pointer = curl_init($url);
                    curl_setopt($pointer, CURLOPT_TIMEOUT, 15);
                    curl_setopt($pointer, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");
                    curl_setopt($pointer, CURLOPT_FOLLOWLOCATION, true);
                    ob_start();
                    curl_exec($pointer);
                    curl_close($pointer);
                    $contents = ob_get_contents();
                    ob_end_clean();
                    break;
            }
            if (eregi('301 Moved Permanently', $contents)) {
                preg_match("/HREF=\"(.*)\"/si", $contents, $pointer);
                return self::open($pointer[1], $method);
            }
            return $contents;
        }

        /**
         * Detecta o tipo MIME do arquivo.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $filename <p>Nome do arquivo completo, com extensão</p>
         * @param string $extensions <p>Extensão</p>
         * @return string <p>Tipo do arquivo, padrão = FALSE | application/octet-stream</p>
         */
        static function mimetype($filename, $extensions) {
            switch (strtolower($extensions)) {
                case "xls":
                    $mimetype = "application/excel";
                    break;
                case "hqx":
                    $mimetype = "application/macbinhex40";
                    break;
                case "doc":
                case "dot":
                case "wrd":
                    $mimetype = "application/msword";
                    break;
                case "pdf":
                    $mimetype = "application/pdf";
                    break;
                case "pgp":
                    $mimetype = "application/pgp";
                    break;
                case "ps":
                case "eps":
                case "ai":
                    $mimetype = "application/postscript";
                    break;
                case "ppt":
                    $mimetype = "application/powerpoint";
                    break;
                case "rtf":
                    $mimetype = "application/rtf";
                    break;
                case "tgz":
                case "gtar":
                    $mimetype = "application/x-gtar";
                    break;
                case "gz":
                    $mimetype = "application/x-gzip";
                    break;
                case "php":
                case "php3":
                    $mimetype = "application/x-httpd-php";
                    break;
                case "js":
                    $mimetype = "application/x-javascript";
                    break;
                case "ppd":
                case "psd":
                    $mimetype = "application/x-photoshop";
                    break;
                case "swf":
                case "swc":
                case "rf":
                    $mimetype = "application/x-shockwave-flash";
                    break;
                case "tar":
                    $mimetype = "application/x-tar";
                    break;
                case "zip":
                    $mimetype = "application/zip";
                    break;
                case "mid":
                case "midi":
                case "kar":
                    $mimetype = "audio/midi";
                    break;
                case "mp2":
                case "mp3":
                case "mpga":
                    $mimetype = "audio/mpeg";
                    break;
                case "ra":
                    $mimetype = "audio/x-realaudio";
                    break;
                case "wav":
                    $mimetype = "audio/wav";
                    break;
                case "bmp":
                    $mimetype = "image/bitmap";
                    break;
                case "gif":
                    $mimetype = "image/gif";
                    break;
                case "iff":
                    $mimetype = "image/iff";
                    break;
                case "jb2":
                    $mimetype = "image/jb2";
                    break;
                case "jpg":
                case "jpe":
                case "jpeg":
                    $mimetype = "image/jpeg";
                    break;
                case "jpx":
                    $mimetype = "image/jpx";
                    break;
                case "png":
                    $mimetype = "image/png";
                    break;
                case "ico":
                    $mimetype = "image/ico";
                    break;
                case "tif":
                case "tiff":
                    $mimetype = "image/tiff";
                    break;
                case "wbmp":
                    $mimetype = "image/vnd.wap.wbmp";
                    break;
                case "xbm":
                    $mimetype = "image/xbm";
                    break;
                case "css":
                    $mimetype = "text/css";
                    break;
                case "txt":
                    $mimetype = "text/plain";
                    break;
                case "htm":
                case "html":
                    $mimetype = "text/html";
                    break;
                case "xml":
                    $mimetype = "text/xml";
                    break;
                case "mpg":
                case "mpe":
                case "mpeg":
                    $mimetype = "video/mpeg";
                    break;
                case "qt":
                case "mov":
                    $mimetype = "video/quicktime";
                    break;
                case "avi":
                    $mimetype = "video/x-ms-video";
                    break;
                case "eml":
                    $mimetype = "message/rfc822";
                    break;
                default:
                    $mimetype = (function_exists("mime_content_type") ? mime_content_type($filename) : false); //"application/octet-stream"
                    break;
            }
            return $mimetype;
        }

    }

}
?>