<?php
require_once(root::path('source/script/tiny_mce/plugins/tinybrowser/config_tinybrowser.php'));

$tbpath = root::host('script/tiny_mce/plugins/tinybrowser'); //$tbpath = pathinfo($_SERVER['SCRIPT_NAME']);
$tbmain = $tbpath . '/tinybrowser.php'; //$tbpath['dirname'].'/tinybrowser.php';
?>

function tinyBrowserPopUp(type,formelementid,folder) {
   tburl = "<?php echo $tbmain; ?>" + "?type=" + type + "&feid=" + formelementid;
   if (folder !== undefined) tburl += "&folder="+folder+"%2F";
   newwindow=window.open(tburl,'tinybrowser','height=<?php echo $tinybrowser['window']['height']+15; ?>,width=<?php echo $tinybrowser['window']['width']+15; ?>,scrollbars=yes,resizable=yes');
   if (window.focus) {newwindow.focus()}
   return false;
}
