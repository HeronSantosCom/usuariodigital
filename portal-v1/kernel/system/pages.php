<?php

if (!function_exists("pages")) {

    /**
     * Retorna array contendo informações de paginação
     * @param int $actual
     * @param int $rows
     * @param int $max
     * @return mixed
     */
    function pages($actual, $rows, $max) {
        $initial = $range = $previous = $next = $pages = false;
        if ($rows > 0) {
            $pages = ceil($rows / $max);
            if ($actual > $pages) {
                $actual = $pages;
            }
            if ($actual > 1) {
                $previous = $actual - 1;
            }
            if ($actual < $pages and $pages > 1) {
                $next = $actual + 1;
            }
            $initial = (($max * $actual) - $max);
            $range = $max;
            return array($initial, $range, $previous, $next, $pages);
        }
        return false;
    }

}
?>
