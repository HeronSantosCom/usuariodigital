<?php

if (!class_exists('socket')) {

    /**
     * Responsável pela conexão socket.
     * @name kengoo/kernel/socket
     * @author Heron Santos
     * @copyright www.heronsantos.com
     */
    class socket {

        protected $server, $port, $timeout;
        public $link;

        /**
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $server <p>Servidor</p>
         * @param integer $port <p>Porta, padrão = 25</p>
         * @param integer $timeout <p>Tempo máximo de resposta, padrão = 45</p>
         * @return void
         */
        public function __construct($server, $port = 25, $timeout = 45) {
            $this->server = $server;
            $this->port = $port;
            $this->timeout = $timeout;
        }
        
        public function __destruct() {
            $this->close();
        }

        /**
         * Conecta com o servidor socket.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @return bool <p>TRUE ou FALSE caso não consiga conectar conexão</p>
         */
        public function open() {
            $this->link = @fsockopen($this->server, $this->port, $errno, $errstr, $this->timeout);
            if ($this->link) {
                kernel::log("Connected at {$this->server}:{$this->port}...", "socket.log");
                socket_set_timeout($this->link, 0, ($this->timeout * 1000));
                return $this->link;
            }
            trigger_error($errstr);
            return false;
        }

        /**
         * Envia um comando ao servidor.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $command <p>Comando</p>
         * @return bool <p>FALSE caso não retorne nenhuma resposta</p>
         */
        public function put($command) {
            if (is_resource($this->link)) {
                $command .= "\r\n";
                $response = fputs($this->link, $command, strlen($command) + 2);
                if ($response) {
                    kernel::log(">>>> {$command}", "socket.log");
                    return $response;
                }
            }
            return false;
        }

        /**
         * Recebe uma resposta ao servidor.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $put <p>Enviar comando, padrão = FALSE</p>
         * @param string $break <p>Ponto de quebra da mensagem, padrão = FALSE</p>
         * @return bool <p>FALSE caso não retorne nenhuma resposta</p>
         */
        public function get($put = false, $break = false) {
            if ($put) {
                if (!$this->put($put)) {
                    return false;
                }
            }
            if (is_resource($this->link)) {
                $continue = true;
                while ($continue) {
                    $response[] = $parser = fgets($this->link);
                    kernel::log("<<<< {$parser}", "socket.log");
                    if (!$break) {
                        if (!(strpos($parser, "\r\n") == false or substr($parser, 3, 1) != ' ')) {
                            $continue = false;
                        }
                    } else {
                        if ($parser == $break) {
                            $continue = false;
                        }
                    }
                }
                if (isset($response)) {
                    return $response;
                }
            }
            return false;
        }

        /**
         * Desconecta com o servidor.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @return bool <p>TRUE ou FALSE caso não consiga desconectar conexão</p>
         */
        public function close() {
            if (is_resource($this->link)) {
                if (fclose($this->link)) {
                    kernel::log("Disconnected from {$this->server}:{$this->port}...", "socket.log");
                    return true;
                }
            }
            return false;
        }

    }

}
?>