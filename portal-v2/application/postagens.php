<?php

class postagens {

    public function __construct($id = false) {
        if ($id) {
            $postagens = new mysqlsearch();
            $postagens->table("postagens_view");
            $postagens->column("*");
            $postagens->match("id", $id);
            $postagens = $postagens->go();
            if (isset($postagens[0])) {
                kernel::extract($postagens[0], false, "postagem");
            }
        }
    }
    
    public function editar () {
        kernel::set("fontes", fontes::listagem());
    }

    public function salvar($id = false) {
        
    }

}

?>
