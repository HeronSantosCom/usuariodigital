<?php

class fontes {

    public function __construct($id = false) {
        if ($id) {
            $fontes = new mysqlsearch();
            $fontes->table("fontes_view");
            $fontes->column("*");
            $fontes->match("id", $id);
            $fontes = $fontes->go();
            if (isset($fontes[0])) {
                kernel::extract($fontes[0], false, "postagem");
            }
        }
    }

    public function salvar($id = false) {
        
    }

    public function apagar($id = false) {
        
    }

    static function listagem($nome = false, $ordenacao = 3, $ordem = 'ASC') {
        $fontes = new mysqlsearch();
        $fontes->table("fontes_view");
        $fontes->column("id");
        $fontes->column("nome");
        $fontes->column("fonte");
        $fontes->column("rss");
        $fontes->column("logotipo");
        if ($nome) {
            $fontes->like("nome", $nome);
        }
        $fontes->order($ordenacao, $ordem);
        return $fontes->go();
    }

    static function editar() {
        
    }

    static function remover() {
        
    }

}

?>
