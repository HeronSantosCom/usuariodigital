<?php

/**
 * Classe de controle de link permanente
 */
class Permalink {

    /**
     * Retorna o link permanente
     * @param int [optional] $index Se declarado retornara a posição, senão retorna um array. Se não for encontrado ou for decladado nenhum link permamente, retornará falso.
     * @return mixed
     */
    static function Get($index = false) {
        Core::Explore();
        if (isset($_ENV["IPS"]["LOCATION"]["PERMALINK"])) {
            if ($index) {
                if (isset($_ENV["IPS"]["LOCATION"]["PERMALINK"][($index - 1)])) {
                    return urldecode($_ENV["IPS"]["LOCATION"]["PERMALINK"][($index - 1)]);
                }
                return false;
            }
            return $_ENV["IPS"]["LOCATION"]["PERMALINK"];
        }
        return false;
    }

    /**
     * Reinicia o ponteiro do link permanente
     * @param int $index ponteiro inicial
     * @return bool
     */
    static function Reset($index) {
        Core::Explore();
        if ($index > 0) {
            if (self::Get($index)) {
                $permalink = $_ENV["IPS"]["LOCATION"]["PERMALINK"];
                for ($x = 0; $x <= ($index - 1); $x++) {
                    array_shift($permalink);
                }
                if (is_array($permalink) and count($permalink) > 0) {
                    $column = 0;
                    foreach ($permalink as $value) {
                        $permalink[$column] = $value;
                        $column++;
                    }
                }
                $_ENV["IPS"]["LOCATION"]["PERMALINK"] = $permalink;
                return true;
            }
        }
        return false;
    }

}

?>
