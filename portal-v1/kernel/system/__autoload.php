<?php

if (!function_exists("__autoload")) {

    /**
     * Carrega a classe automaticamente antes de ser instanciada.
     * @param string $class_name
     */
    function __autoload($class_name) {
        if (!class_exists($class_name)) {
            $class_name = strtolower($class_name);
            if (file_exists("applications/{$class_name}.php")) {
                include "applications/{$class_name}.php";
            } else {
                if (file_exists("extras/{$class_name}.php")) {
                    include "extras/{$class_name}.php";
                } else {
                    if (file_exists("kernel/{$class_name}.php")) {
                        include "kernel/{$class_name}.php";
                    } else {
                        reporting("404", "Aplicação requisitada {$class_name} não foi encontrada.");
                    }
                }
            }
        }
    }

}
?>
