/***************************
	  CUSTOM JQUERY
***************************/

// FUNCTIONS

// Fix height of the Container

var calcContainerHeight = function() {
    var headerDimensions = $('#header').height();
    $('#container').height($(window).height() - headerDimensions);
}
  
// Fix height of Content section

var calcContentHeight = function() {
    var headerDimensions = $('#header').height();
    var subheaderDimensions = $('#subheader').height();
    $('#content').height($(window).height() - headerDimensions - subheaderDimensions -1);
}
  
// Make sure the whole of the sidebar menu is always displayed

var calcSidebarHeight = function() {
    var buttonDimensions = $('a.menu').outerHeight();
    var navigationDimensions = $('ul.navigation.visible').outerHeight();
    $('div#sidebar').css('min-height', buttonDimensions + navigationDimensions+ 50);
}
	
// Set up sidebar menu basics

var SidebarMenu = function() {
    if( $('ul.navigation li.current').length>0 ){
        var CurrentNavigation = $('html').find('ul.navigation li.current').parent().attr('id');
        var CurrentNavigationName = $('a[href="'+CurrentNavigation+'"]').html();
        $('a.menu').empty().append(CurrentNavigationName);
        $('ul.navigation').hide();
        $('ul.navigation li.current').parent().addClass('visible').show();
    }
    else{
        var FirstNavigation = $('html').find('ul.navigation:first').attr('id');
        var FirstNavigationName = $('a[href="'+FirstNavigation+'"]').html();
        $('ul#'+FirstNavigation).addClass('current');
        $('a.menu').empty().append(FirstNavigationName);
        $('ul.navigation:not(.current)').hide();
    }
    $('ul.navigation li ul').hide(); //Hide all sub nav's
    if( $('ul.navigation li.current:contains(ul)')){
        $('ul.navigation li.current ul').show();
    }
}
	
// MAIN JQUERY
  
$(document).ready(function() {
	
    // Run functions on window load, and resize
	
    $(window).resize(function() {
        calcContainerHeight();
        calcContentHeight();
    }).load(function() {
        calcContainerHeight();
        calcContentHeight();
        SidebarMenu();
        calcSidebarHeight();
    });
		
    // Make dropdown menu work on click
		
    $('a.menu').click(function () {
        $('ul#menu').slideDown('fast');
        return false;
    });
		
    // Hide dropdown when user clicks outside of it
	
    $('body').click(function() {
        $('ul#menu:visible').hide();
    });
		
    // Display the correct menu when picked from dropdown menu
	
    $('ul#menu li a').click(function (){
        $('ul#menu:visible').hide();
        var NavigationDestination = $(this).attr('href');
        if( $('ul#'+NavigationDestination).is(':visible') ) {
        }
        else{
            $('a.menu').empty().append(NavigationDestination);
            $('ul.navigation').hide();
            $('ul#'+NavigationDestination).slideDown('fast', function() {
                calcSidebarHeight();
            });
        }
        return false;
    });
		
    // If sub navigation exists, display when the link is clicked, if it's already visible do nothing, if there is no sub navigation, the link functions normally
	
    $('ul.navigation li a').not('ul.navigation li ul li a').click(function (){
        if($(this).siblings('ul:not(:visible)').length){
            $('ul.navigation li ul').slideUp('medium');
            $(this).siblings('ul').slideDown('medium', function(){
                calcSidebarHeight();
            });
            return false;
        }
        else if ($(this).siblings('ul:visible').length){
            return false;
        }
        else{
    // Maybe do some more stuff in here...
    }
    });
		
    // Fire the Uniform Script if checkboxes, radio buttons, or file inputs are found
	
    var applyUniform = function() {
        //$.getScript('js/jquery.uniform.min.js', function() {
        $.getScript(GetLocal('gallery/js/jquery.uniform.js'), function() {
            $("input:checkbox, input:radio").uniform();
        //$('input:file').after('<div class="clear"></div>')
        });
    }
							
    if ($('input:checkbox').length > 0){
        applyUniform();
    }
    else if ($('input:radio').length > 0){
        applyUniform();
    }
    //    else if ($('input:file').length > 0){
    //        applyUniform();
    //    }

    // Close notifications (fade and slideup)
		
    $(".notification a.close").click(function () {
        $(this).parent().fadeTo(400, 0, function () {
            $(this).slideUp(200);
        });
        return false;
    });
		
    // Apply table sorter if a table with class tablesorter is found ( in the examples case, dont sort column 1 or 5)
		
    if ($('table.tablesorter')) {
        //$.getScript('js/jquery.tablesorter.min.js', function() {
        $.getScript(GetLocal('gallery/js/jquery.tablesorter.js'), function() {
            $("table.tablesorter").tablesorter({
                headers: {
                    0: {
                        sorter: false
                    },
                    4: {
                        sorter: false
                    }
                }
            });
        });
    }
		
    // Table row hover highlighter and show links
	
    $('table.hover tbody tr').hover(
        function() {  // mouseover
            $(this).children('td').addClass('hover');
            $(this).children('td:nth-child(2)').append('<p class="links"><a href="#">Edit</a><a href="#">Preview</a><a href="#">Delete</a></p>');
        },
        function() {  // mouseout
            $(this).children('td').removeClass('hover');
            $(this).children('td:nth-child(2)').find('p.links').remove();
        }
        );
		   
    // Check all checkboxes in the table when the header checkbox is selected
	
    $('table thead input[type=checkbox]').click(function(){
        var table = $(this).parents('table');
        if($(this).is(':checked')){
            table.find('div.checker span').addClass('checked');
            table.find('input[type=checkbox]').attr('checked', true);
        }
        else if($(this).is(':not(:checked)')){
            table.find('div.checker span').removeClass('checked');
            table.find('input[type=checkbox]').attr('checked', false);
        }
    });
		
		
    // Textarea limiter

    if ($('textarea.limit').length > 0){
        //$.getScript('js/jquery.limit-1.2.js', function() {
        $.getScript(GetLocal('gallery/js/jquery.limit.js'), function() {
            $('textarea.limit').limit('150','#charsLeft');
        });
    }

    $(document).ready(function() {
        $('.switable').addClass("input-idle");
        $('.switable').focus(function() {
            $(this).removeClass("input-idle").addClass("input-focus");
            if (this.value == this.defaultValue){
                this.value = '';
            }
            if(this.value != this.defaultValue){
                this.select();
            }
        });
        $('.switable').blur(function() {
            $(this).removeClass("input-focus").addClass("input-idle");
            if ($.trim(this.value == '')){
                this.value = (this.defaultValue ? this.defaultValue : '');
            }
        });
    });
  	
});
