<?php

class imagem extends control {

    public function __construct() {
        $this->extract($_GET);
        if (isset($this->avatar)) {
            $this->avatar($this->avatar);
        }
    }

    public function avatar($usuario) {
        
        $file = "avatar/avatar.jpg";
        $usuarios = new mysqlsearch();
        $usuarios->table("usuarios_view");
        $usuarios->column("avatar");
        $usuarios->match("id", $usuario);
        $usuarios = $usuarios->go();
        if (isset($usuarios[0]["avatar"])) {
            //dump(path::local("avatar/{$usuarios[0]["avatar"]}"));
            //exit;
            if (file_exists(path::local("/avatar/{$usuarios[0]["avatar"]}"))) {
                $file = "avatar/128x128_{$usuarios[0]["avatar"]}";
                if (!file_exists(path::local("/avatar/128x128_{$usuarios[0]["avatar"]}"))) {
                    $thumb = phpthumbfactory::create(path::local("avatar/{$usuarios[0]["avatar"]}"));
                    $thumb->resize(300, 300)->cropFromCenter(128, 128);
                    $thumb->save(path::local("/avatar/128x128_{$usuarios[0]["avatar"]}"));
                }
            }
        }
        $this->redirect($file);
    }

}

?>