<?php
if (!function_exists("error")) {

    /**
     * Utilizado pelo error_handler do PHP, cria um evento de erro no sistema.
     * @author Heron Santos
     * @copyright www.heronsantos.com
     * @param integer $code <p>Código do erro, padrão = 500</p>
     * @param string $message <p>Mensagem do erro, padrão = FALSE</p>
     * @return void
     */
    function error($code = 500, $message = false) {
        if ($message) {
            kernel::trace();
            switch ($code) {
                case 100:
                    $codemsg = "100 Continue";
                    break;
                case 101:
                    $codemsg = "101 Switching Protocols";
                    break;
                case 200:
                    $codemsg = "200 OK";
                    break;
                case 201:
                    $codemsg = "201 Created";
                    break;
                case 203:
                    $codemsg = "203 Non-Authoritative Information";
                    break;
                case 204:
                    $codemsg = "204 No Content";
                    break;
                case 205:
                    $codemsg = "205 Reset Content";
                    break;
                case 206:
                    $codemsg = "206 Partial Content";
                    break;
                case 300:
                    $codemsg = "300 Multiple Choices";
                    break;
                case 301:
                    $codemsg = "301 Moved Permanently";
                    break;
                case 302:
                    $codemsg = "302 Found";
                    break;
                case 303:
                    $codemsg = "303 See Other";
                    break;
                case 304:
                    $codemsg = "304 Not Modified";
                    break;
                case 305:
                    $codemsg = "305 Use Proxy";
                    break;
                case 306:
                    $codemsg = "306 (Unused)";
                    break;
                case 307:
                    $codemsg = "307 Temporary Redirect";
                    break;
                case 400:
                    $codemsg = "400 Bad Request";
                    break;
                case 401:
                    $codemsg = "401 Unauthorized";
                    break;
                case 402:
                    $codemsg = "402 Payment Required";
                    break;
                case 403:
                    $codemsg = "403 Forbidden";
                    break;
                case 404:
                    $codemsg = "404 Not Found";
                    break;
                case 405:
                    $codemsg = "405 Method Not Allowed";
                    break;
                case 406:
                    $codemsg = "406 Not Acceptable";
                    break;
                case 407:
                    $codemsg = "407 Proxy Authentication Required";
                    break;
                case 408:
                    $codemsg = "408 Request Timeout";
                    break;
                case 409:
                    $codemsg = "409 Conflict";
                    break;
                case 410:
                    $codemsg = "410 Gone";
                    break;
                case 411:
                    $codemsg = "411 Length Required";
                    break;
                case 412:
                    $codemsg = "412 Precondition Failed";
                    break;
                case 413:
                    $codemsg = "413 Request Entity Too Large";
                    break;
                case 414:
                    $codemsg = "414 Request-URI Too Long";
                    break;
                case 415:
                    $codemsg = "415 Unsupported Media Type";
                    break;
                case 416:
                    $codemsg = "416 Requested Range Not Satisfiable";
                    break;
                case 417:
                    $codemsg = "417 Expectation Failed";
                    break;
                case 500:
                default:
                    $codemsg = "500 Internal Server Error";
                    break;
                case 501:
                    $codemsg = "501 Not Implemented";
                    break;
                case 502:
                    $codemsg = "502 Bad Gateway";
                    break;
                case 503:
                    $codemsg = "503 Service Unavailable";
                    break;
                case 504:
                    $codemsg = "504 Gateway Timeout";
                    break;
                case 505:
                    $codemsg = "505 HTTP Version Not Supported";
                    break;
            }
            header("HTTP/1.0 {$codemsg}");
            header("Status: {$codemsg}");
            $_SERVER['REDIRECT_STATUS'] = (int) $code;
            throw new Exception((is_array($message) ? join("\n", $message) : $message));
        }
    }

}
?>