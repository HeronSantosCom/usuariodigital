<?php

class Gallery {

    private $ids, $search, $options;
    private $page = 1;

    public function __construct() {
        Index::MySQL();
        System::Set("Title", "Galeria");
        if (isset($_GET["options"])) {
            $this->options = $_GET["options"];
        }
    }

    public function SetId($id = false) {
        return $this->ids = ($id ? $id : Permalink::Get(1));
    }

    public function SetPage($page = false) {
        return $this->page = ($page ? $page : (Permalink::Get(1) ? Permalink::Get(1) : 1));
    }

    public function SetSearch($search = false) {
        if (isset($_POST["search"])) {
            $this->search = $_POST["search"];
            $this->page = 1;
            System::Set("Redirect", "admin/internal/dashboard/gallery/1/{$this->search}", "Admin");
            return true;
        }
        return $this->search = ($search ? $search : Permalink::Get(2));
    }

    public function Upload() {
        if (!empty($_FILES)) {
            $tmp_pathinfo = pathinfo($_FILES['Filedata']["tmp_name"]);
            $tmp_basename = $tmp_pathinfo['basename'];
            if (move_uploaded_file($_FILES['Filedata']["tmp_name"], Core::Local("cache/{$tmp_basename}"))) {
                print base64_encode(serialize($_FILES['Filedata']));
            }
        }
    }

    public function GetFiles($ids) {
        $search = new MySQLSearch();
        $search->Table("ips_gallery");
        $search->Column("file");
        $search->Column("thumb");
        $search->In("id", explode(",", $this->ids));
        return $search->Go();
    }

    public function RemoveFiles($files) {
        if (is_array($files)) {
            foreach ($files as $file) {
                if (file_exists(Core::Local($file["file"]))) {
                    unlink(Core::Local($file["file"]));
                }
                if (strlen($file["thumb"]) > 0) {
                    if (file_exists(Core::Local($file["thumb"]))) {
                        unlink(Core::Local($file["thumb"]));
                    }
                }
            }
        }
    }

    public function Save() {
        System::Set("status", "1", "Form");
        System::Set("ips_account_id", System::Get("id", "Authenticate"), "Form");
        if (isset($_POST["ips_confirm"])) {
            $this->Clean();
            System::Extract($_POST, false, false, "Form");
            $saved = (!$this->ids ? $this->Insert() : $this->Update($this->ids));
            if ($saved) {
                System::Redirect(Core::Remote(System::Get("Redirect", "Admin")));
                Admin::Alert("Registro salvado!");
                return true;
            }
            Admin::Alert("Problemas ao efetuar operação!");
        }
        return false;
    }

    public function Delete() {
        if ($this->ids) {
            $files = $this->GetFiles($this->ids);
            $delete = new MySQLDelete();
            $delete->Table("ips_gallery");
            $delete->In("id", explode(",", $this->ids));
            if ($delete->Go()) {
                $this->RemoveFiles($files);
                System::Redirect(Core::Remote(System::Get("Redirect", "Admin")));
                Admin::Alert("Registro(s) removido(s)!");
                return true;
            }
        }
        Admin::Alert("Problemas ao efetuar operação!");
        return false;
    }

    public function Open() {
        if ($this->ids) {
            $search = new MySQLSearch();
            $search->Table("ips_gallery");
            $search->Column("*");
            $search->Match("id", $this->ids);
            $search->Limit(1);
            $search = $search->Go();
            if ($search) {
                System::Extract($search[0], null, null, "Form");
                return true;
            }
            Admin::Alert("Registro não encontrado!");
        }
        return false;
    }

    public function Search() {
        $search = new MySQLSearch();
        $search->Table("ips_gallery");
        $search->Column("id");
        $search->Column("title");
        $search->Column("description");
        $search->Column("status");
        $search->Column("DATE_FORMAT(`insert`, '%d/%m/%Y %H:%i')", false, "insert_date");
        if ($this->search) {
            $search->Like("title", "%{$this->search}%");
            $search->Like("description", "%{$this->search}%", "OR");
            $search->Like("metatag", "%{$this->search}%", "OR");
            $search->Like("DATE_FORMAT(`insert`, '%d/%m/%Y %H:%i')", "%{$this->search}%", "OR");
            System::Set("Search Text", $this->search);
        }
        $search->Order(2);
        $search->limit = $this->Pagination($search->where);
        $search = $search->Go();
        if ($search) {
            return System::Set("Search", $search);
        }
        return false;
    }

    public function Options() {
        $search = new MySQLSearch();
        $search->Table("ips_gallery");
        $search->Column("id");
        $search->Column("title");
        $search->Order(2);
        if ($this->options) {
            $search->Like("title", "%{$this->options}%");
        }
        System::Set("Options Gallery", $search->Go());
    }

    private function Pagination($where) {
        $total = new MySQLSearch();
        $total->Table("ips_gallery");
        $total->Column("COUNT(id)", false, "total");
        $total->where = $where;
        $pagination = System::Pages($total, $this->page, admin_limit);
        if ($pagination) {
            System::Set("Search Pagination", $pagination[1]);
            return $pagination[0];
        }
        return false;
    }

    private function Clean() {
        $clean = new MySQLShow();
        $clean->Table("ips_gallery");
        $fields = $clean->Columns();
        if ($fields) {
            foreach ($fields as $field) {
                System::Set($field["Field"], false, "Form");
            }
        }
    }

    private function Insert() {
        if (System::Get("file", "Form")) {
            $tmp = unserialize(base64_decode(trim(System::Get("file", "Form"))));
            if (is_array($tmp)) {
                $tmp_pathinfo = pathinfo($tmp["tmp_name"]);
                $tmp_basename = $tmp_pathinfo['basename'];
                $pathinfo = pathinfo($tmp["name"]);
                $basename = $pathinfo['basename'];
                $extension = (isset($pathinfo['extension']) ? $pathinfo['extension'] : "unk");
                $mimetype = System::MimeType($tmp["name"]);
                $size = $tmp["size"];
                $file = md5(date("r") . @session_id()) . strtolower(".{$extension}");
                if (@rename(Core::Local("cache/{$tmp_basename}"), Core::Local("gallery/db/{$file}"))) {
                    $thumb = false;
                    if (preg_match("#image#", $mimetype)) {
                        $thumb = new Thumbnail(Core::Local("gallery/db/{$file}"));
                        $thumb->size_auto(210);
                        $thumb->output_format = 'png';
                        $thumb->process();
                        if ($thumb->save(Core::Local("gallery/db/thumbs/{$file}"))) {
                            $thumb = "gallery/db/thumbs/{$file}";
                        }
                    }
                    $metatag = System::KeyWords(System::Get("description", "Form"));
                    if (is_array($metatag)) {
                        $metatag = join(", ", array_keys($metatag));
                    }
                    $save = new MySQLSave();
                    $save->Table("ips_gallery");
                    $save->Column("title", System::Get("title", "Form"));
                    $save->Column("description", System::Get("description", "Form"));
                    $save->Column("file", "gallery/db/{$file}");
                    $save->Column("thumb", $thumb);
                    $save->Column("metatag", $metatag);
                    $save->Column("mimetype", $mimetype);
                    $save->Column("size", $size);
                    $save->Column("status", System::Get("status", "Form"));
                    $save->Column("insert", date("Y-m-d H:i:s"));
                    $save->Column("ips_account_id", System::Get("ips_account_id", "Form"));
                    $save->Column("ips_category_id", System::Get("ips_category_id", "Form"));
                    if ($save->Go()) {
                        return $save->GetID();
                    }
                }
                if (file_exists($tmp["tmp_name"])) {
                    unlink($tmp["tmp_name"]);
                }
                if (file_exists(Core::Local("cache/{$tmp_basename}"))) {
                    unlink(Core::Local("cache/{$tmp_basename}"));
                }
                if (file_exists(Core::Local("gallery/db/{$file}"))) {
                    unlink(Core::Local("gallery/db/{$file}"));
                }
                if (file_exists(Core::Local("gallery/db/thumbs/{$file}"))) {
                    unlink(Core::Local("gallery/db/thumbs/{$file}"));
                }
            }
        }
        return false;
    }

    private function Update($id) {
        $metatag = System::KeyWords(System::Get("description", "Form"));
        if (is_array($metatag)) {
            $metatag = join(", ", array_keys($metatag));
        }
        $save = new MySQLSave();
        $save->Table("ips_gallery");
        $save->Column("title", System::Get("title", "Form"));
        $save->Column("description", System::Get("description", "Form"));
        $save->Column("metatag", $metatag);
        $save->Column("status", System::Get("status", "Form"));
        $save->Column("ips_account_id", System::Get("ips_account_id", "Form"));
        $save->Column("ips_category_id", System::Get("ips_category_id", "Form"));
        $save->Match("id", $id);
        if ($save->Go()) {
            return $id;
        }
        return false;
    }

}

?>