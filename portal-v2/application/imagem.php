<?php

class imagem {

    public function __construct() {
        kernel::extract($_GET);
        if (kernel::get("avatar")) {
            $this->avatar(kernel::get("avatar"));
        }
    }

    public function avatar($usuario) {
        $file = "imagens/avatar/avatar.jpg";
        $usuarios = new mysqlsearch();
        $usuarios->table("usuarios_view");
        $usuarios->column("avatar");
        $usuarios->match("id", $usuario);
        $usuarios = $usuarios->go();
        if (isset($usuarios[0]["avatar"])) {
            if (file_exists(root::path("source/imagens/avatar/{$usuarios[0]["avatar"]}"))) {
                $file = "imagens/avatar/128x128_{$usuarios[0]["avatar"]}";
                if (!file_exists(root::path("source/imagens/avatar/128x128_{$usuarios[0]["avatar"]}"))) {
                    $thumb = phpthumbfactory::create(root::path("source/imagens/avatar/{$usuarios[0]["avatar"]}"));
                    $thumb->resize(300, 300)->cropFromCenter(128, 128);
                    $thumb->save(root::path("source/imagens/avatar/128x128_{$usuarios[0]["avatar"]}"));
                }
            }
        }
        source::get($file);
        exit(0);
    }

}

?>