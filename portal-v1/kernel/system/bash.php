<?php

if (!function_exists("bash")) {

    /**
     * Determina a pasta para manipulação de arquivo
     * @return bool
     */
    function bash() {
        if (!isset($_SERVER["HTTP_HOST"])) {
            if (isset($_SERVER["argv"])) {
                $path = explode("/", $_SERVER["argv"][0]);
                array_pop($path);
                $path = join("/", $path);
                if (strlen($path) > 0) {
                    chdir($path);
                }
                return true;
            }
        }
        return false;
    }

}

bash();
?>
