<?php

if (!class_exists('compose')) {
    
    /**
     * Classe responsável pelo tratamendo do HTML.
     * @name kengoo/kernel/compose
     * @author Heron Santos
     * @copyright www.heronsantos.com
     */
    class compose {

        /**
         * Questiona o o código tratando se for o caso de acordo com cada instrução.
         * <ul>
         * <li>
         * <b>run:</b> <i>instancia uma aplicação, exemplo:</i><br />
         * <code>
         * <!-- run (class) -->
         * </code>
         * </li>
         * <li>
         * <b>run function:</b> <i>instancia uma aplicação e executa uma função, exemplo:</i><br />
         * <code>
         * <!-- run (class function) -->
         * </code>
         * </li>
         * <li>
         * <b>set:</b> <i>declara uma variável para o sistema, exemplo:</i><br />
         * <code>
         * <!-- set (name) value -->
         * </code>
         * </li>
         * <li>
         * <b>get:</b> <i>resgata e imprime uma variável, exemplo:</i><br />
         * <code>
         * <!-- get (name) -->
         * </code>
         * </li>
         * <li>
         * <b>get parent:</b> <i>resgata e imprime uma variável de uma alocação, exemplo:</i><br />
         * <code>
         * <!-- get (name) parent -->
         * </code>
         * </li>
         * <li>
         * <b>html:</b> <i>resgata e imprime uma variável contendo um conteúdo HTML, exemplo:</i><br />
         * <code>
         * <!-- html (name) -->
         * </code>
         * </li>
         * <li>
         * <b>html parent:</b> <i>resgata e imprime uma variável contendo um conteúdo HTML de uma alocação, exemplo:</i><br />
         * <code>
         * <!-- html (name) parent -->
         * </code>
         * </li>
         * <li>
         * <b>constant:</b> <i>resgata e imprime uma constante definida no sistema, exemplo:</i><br />
         * <code>
         * <!-- constant (name) -->
         * </code>
         * </li>
         * <li>
         * <b>if:</b> <i>similiar ao IF do PHP e de outras linguagem, permite a execução condicional de fragmentos de código, exemplo:</i><br />
         * <code>
         * <!-- if (expr) name -->
         * html code if true
         * <!-- end(name) -->
         * </code>
         * expr:<ul>
         * <li>a == b (A igual a B)</li>
         * <li>a != b (A diferente b)</li>
         * <li>a >= b (A maior ou igual a B)</li>
         * <li>a <= b (A menor ou igual a B)</li>
         * <li>a && b <i>(se B contem em A, podendo ser múltimo serapando por | )</i></li>
         * </ul>
         * </li>
         * <li>
         * <b>if else:</b> <i>mesmo que o comando if com o tratamento de negação, exemplo:</i><br />
         * <code>
         * <!-- if (expr) name -->
         * html code if true
         * <!-- else(name) -->
         * html code if false
         * <!-- end(name) -->
         * </code>
         * expr:<ul>
         * <li>a == b (A igual a B)</li>
         * <li>a != b (A diferente b)</li>
         * <li>a >= b (A maior ou igual a B)</li>
         * <li>a <= b (A menor ou igual a B)</li>
         * <li>a && b <i>(se B contem em A, podendo ser múltimo serapando por | )</i></li>
         * </ul>
         * </li>
         * <li>
         * <b>for:</b> <i>repete o código obedecendo os ponteiros iniciais e finais, exemplo:</i><br />
         * <code>
         * <!-- for (start,end) name -->
         * $name() = imprime o ponteiro atual
         * <!-- end(name) -->
         * </code>
         * </li>
         * <li>
         * <b>each:</b> <i>resgata uma variável do tipo array (bidimensional) repetindo o código, exemplo:</i><br />
         * <code>
         * <!-- each (var) name -->
         * $name() = imprime o ponteiro atual
         * $name(key) = imprime o valor definido na chave (key)
         * $name(this) = imprime o valor
         * <!-- end(name) -->
         * </code>
         * </li>
         * <ul>
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @todo Precisa resolver o problema caso use mais de um obj com o memso nome.
         * @param string $source <p>Código a ser questionado e tratado</p>
         * @return string <p>Código tratado</p>
         */
        static function get($source) {
            self::stop_looping($source);
            if (preg_match('/\<\!\-\-*([^\-\-\>]+?)\-\-\>/i', $source, $struction)) {
                if (isset($struction[1])) {
                    if (preg_match('/(.*?)[\(](.*)[\)](.*)\s/i', $struction[1], $function)) {
                        if (isset($function[2])) {
                            $decode = false;
                            switch (trim($function[1])) {
                                case "run":
                                    $class = explode(" ", trim(str_replace(array("-", "/", ">"), " ", $function[2])));
                                    if (isset($class[1])) {
                                        $static = $class[1];
                                        $class = $class[0];
                                        if (!kernel::get("osoc-{$class}")) {
                                            $parser = new $class();
                                            kernel::set("osoc-{$class}", $parser);
                                        } else {
                                            $parser = kernel::get("osoc-{$class}");
                                        }
                                        $parser->$static();
                                    } else {
                                        $class = $class[0];
                                        if (!kernel::get("osoc-{$class}")) {
                                            $parser = new $class();
                                        }
                                    }
                                    break;
                                case "set":
                                    $value = null;
                                    $path = trim($function[2]);
                                    if (isset($function[3]) and strlen(trim($function[3])) > 0) {
                                        kernel::set(trim($function[3]), $path);
                                    }
                                    break;
                                case "constant":
                                    $value = constant(trim($function[2]));
                                    if (is_array($value)) {
                                        $value = "1";
                                    }
                                    break;
                                case "html":
                                    $decode = true;
                                case "get":
                                    if (isset($function[3]) and strlen(trim($function[3])) > 0) {
                                        $value = kernel::get(trim($function[2]), trim($function[3]));
                                    } else {
                                        $value = kernel::get(trim($function[2]));
                                    }
                                    if (is_array($value)) {
                                        $value = "1";
                                    }
                                    if (!$decode) {
                                        $value = htmlentities($value, ENT_COMPAT, system_interface_encoding);
                                    }
                                    break;
                                case "if":
                                    $source = self::so($function, $source);
                                    break;
                                case "for":
                                    $source = self::to($function, $source);
                                    break;
                                case "each":
                                    $source = self::each($function, $source);
                                    break;
                            }
                        }
                    }
                    $source = self::get(str_replace($struction[0], (isset($value) ? $value : false), $source));
                }
            }
            return $source;
        }

        /**
         * Semelhante ao comando FOR do PHP.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $condition <p>Condição para o tratamento</p>
         * @param string $haystack <p>Código a ser tratamento</p>
         * @return string <p>Código tratado</p>
         */
        static private function to($condition, $haystack) {
            $function = $condition[0];
            $name = trim($condition[3]);
            $condition = trim($condition[2]);
            $init = strpos($haystack, "<!--{$function}-->");
            $parser = substr($haystack, $init);
            $end = strpos($parser, "<!-- end({$name}) -->") + strlen("<!-- end({$name}) -->");
            $parser = substr($parser, 0, $end);
            $offset = substr($parser, strlen("<!--{$function}-->"), -strlen("<!-- end({$name}) -->"));
            if (preg_match('/(.*),(.*)/i', $condition, $function)) {
                if (isset($function[2])) {
                    $start = trim($function[1]);
                    $end = trim($function[2]);
                    if ($end >= $start) {
                        for ($x = $start; $x <= $end; $x++) {
                            $replaced = str_replace('$' . $name . '()', $x, $offset);
                            $replace[] = $replaced;
                        }
                    }
                    $haystack = str_replace($parser, (isset($replace) ? join("", $replace) : null), $haystack);
                }
            }
            return $haystack;
        }

        /**
         * Semelhante ao comando FOREACH do PHP.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $condition <p>Condição para o tratamento</p>
         * @param string $haystack <p>Código a ser tratamento</p>
         * @return string <p>Código tratado</p>
         */
        static private function each($condition, $haystack) {
            $function = $condition[0];
            $name = trim($condition[3]);
            $condition = trim($condition[2]);
            $init = strpos($haystack, "<!--{$function}-->");
            $parser = substr($haystack, $init);
            $end = strpos($parser, "<!-- end({$name}) -->") + strlen("<!-- end({$name}) -->");
            $parser = substr($parser, 0, $end);
            $offset = substr($parser, strlen("<!--{$function}-->"), -strlen("<!-- end({$name}) -->"));
            if (preg_match('/(.*)/i', $condition, $function)) {
                if (isset($function[1])) {
                    $array = kernel::get(trim($function[1]));
                    if (is_array($array)) {
                        foreach ($array as $key => $value) {
                            $replaced = str_replace('$' . $name . '()', $key, $offset);
                            if (is_array($value)) {
                                foreach ($value as $field => $val) {
                                    $val = htmlentities($val, ENT_COMPAT, system_interface_encoding);
                                    $replaced = str_replace('$' . $name . '(' . $field . ')', (is_array($val) ? null : $val), $replaced);
                                }
                            } else {
                                $value = htmlentities($value, ENT_COMPAT, system_interface_encoding);
                                $replaced = str_replace('$' . $name . '(this)', $value, $replaced);
                            }
                            $replace[] = $replaced;
                        }
                    }
                    $haystack = str_replace($parser, (isset($replace) ? join("", $replace) : null), $haystack);
                }
            }
            return $haystack;
        }

        /**
         * Semelhante ao comando IF do PHP.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $condition <p>Condição para o tratamento</p>
         * @param string $haystack <p>Código a ser tratamento</p>
         * @return string <p>Código tratado</p>
         */
        static private function so($condition, $haystack) {
            $function = $condition[0];
            $name = trim($condition[3]);
            $condition = trim($condition[2]);
            $init = strpos($haystack, "<!--{$function}-->");
            $parser = substr($haystack, $init);
            $else = strpos($parser, "<!-- else({$name}) -->");
            $end = strpos($parser, "<!-- end({$name}) -->");
            $parser = substr($parser, 0, $end + strlen("<!-- end({$name}) -->"));
            $parser_1 = substr($parser, strlen("<!--{$function}-->"), $end);
            $parser_2 = null;
            if (strlen($else) > 0) {
                $parser_1 = substr($parser, strlen("<!--{$function}-->"), $else - strlen("<!--{$function}-->"));
                $parser_2 = substr($parser, $else + strlen("<!-- else({$name}) -->"), $end - ($else + strlen("<!-- else({$name}) -->")));
            }
            if (preg_match('/(.*)(.[\!\=|\=\=|\>|\>\=|\<|&&|\<\=])(.*)/i', $condition, $function)) {
                if (isset($function[3])) {
                    $x = trim($function[1]);
                    $decision = trim($function[2]);
                    $y = trim($function[3]);
                    $offset = false;
                    switch ($decision) {
                        case '==':
                            if ($x == $y) {
                                $offset = true;
                            }
                            break;
                        case '&&':
                            if (preg_match("#($y)#", $x)) {
                                $offset = true;
                            }
                            break;
                        case '!=':
                            if ($x != $y) {
                                $offset = true;
                            }
                            break;
                        case '>=':
                            if ((double) $x >= (double) $y) {
                                $offset = true;
                            }
                            break;
                        case '<=':
                            if ((double) $x <= (double) $y) {
                                $offset = true;
                            }
                            break;
                    }
                    $haystack = str_replace($parser, ($offset ? $parser_1 : $parser_2), $haystack);
                }
            }
            return $haystack;
        }

        /**
         * Evita o looping infinito causando descarga de memória do servidor em caso de execução acidental.
         * @static
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $parser <p>Comparativo</p>
         * @return mixed <p>Retorna verdadeiro se o comparativo for válido, caso contrário irá encerrar o sistema</p>
         */
        static private function stop_looping($parser) {
            $parser = md5($parser);
            if (kernel::get("slc-{$parser}") > 3) { //Stop Looping Control
                trigger_error("The system detected an illegal operation and blocked the execution ...");
                return die();
            }
            return kernel::set("slc-{$parser}", (kernel::get("slc-{$parser}") + 1));
        }

    }

}
?>