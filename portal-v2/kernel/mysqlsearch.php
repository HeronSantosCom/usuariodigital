<?php

if (!class_exists('mysqlsearch')) {


    /**
     * Consulta na base de dados MySQL.
     * @name kengoo/kernel/mysql/mysqlsearch
     * @author Heron Santos
     * @todo dividir os indices em array, contendo os itens e alias
     * @copyright www.heronsantos.com
     */
    class mysqlsearch extends mysql {

        private $index = false;
        private $result = false;
        public $database = false;
        public $table = false;
        public $columns = false;
        public $where = false;
        public $group = false;
        public $having = false;
        public $order = false;
        public $limit = false;

        public function __construct() {
            parent::__construct();
        }

        /**
         * Seta o banco de dados.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $name <p>Bando de dados</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function database($name) {
            $index = $this->index("database");
            $this->database[$index] = $this->ereaser($name, "`");
            return $this;
        }

        /**
         * Seta a tabela.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $name <p>Tabela</p>
         * @param integer $databasekey <p>Banco de dados (chave de indicação), padrão = 1</p>
         * @param string $alias <p>Apelido para tabela, padrão = FALSE</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function table($name, $databasekey = 1, $alias = false) {
            $database = (isset($this->database[$databasekey]) ? "{$this->database[$databasekey]}." : $this->ereaser($this->select, "`") . ".");
            $table = $this->ereaser($name, "`");
            $index = $this->index("table");
            $alias = ($alias ? $alias : $this->ereaser("t{$index}", "`"));
            $this->table[$index] = array(false, "{$database}{$table} {$alias}", $alias);
            return $this;
        }

        /**
         * Unir registros de tabela.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $name <p>Tabela</p>
         * @param array $condition <p>Condição (coluna da tabela 1, operador, coluna da tabela 2, tabela da coluna 1 (opcional), tabela da coluna 2 (opcional)), padrão = FALSE</p>
         * @param string $factor [ LEFT | RIGHT | INNER | FALSE ] <p>Fator de união das tabelas, padrão = FALSE</p>
         * @param integer $databasekey <p>Banco de dados (chave de indicação), padrão = 1</p>
         * @param string $alias <p>Apelido para tabela, padrão = FALSE</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function join($name, $condition = false, $factor = false, $databasekey = 1, $alias = false) {
            $index_condition = $this->index("table", true);
            $database = (isset($this->database[$databasekey]) ? "{$this->database[$databasekey]}." : $this->ereaser($this->select, "`") . ".");
            $table = $this->ereaser($name, "`");
            $index = $this->index("table");
            $alias = ($alias ? $alias : $this->ereaser("t{$index}", "`"));

            switch ($factor) {
                case "LEFT":
                    $join = "LEFT JOIN";
                    break;
                case "RIGHT":
                    $join = "RIGHT JOIN";
                    break;
                case "INNER":
                    $join = "INNER JOIN";
                    break;
                default:
                    if (!$factor) {
                        $join = "JOIN";
                    } else {
                        trigger_error("Factor to do a JOIN in your query is not allowed!");
                        return false;
                    }
                    break;
            }

            $on = null;
            if ($condition) {
                if (is_array($condition)) {
                    if (!isset($this->table[$index_condition])) {
                        trigger_error("Table for comparison previously unreported!");
                        return false;
                    }
                    $column_condition_1 = $this->ereaser($condition[0], "`");
                    $column_condition_2 = (isset($condition[2]) ? $this->ereaser($condition[2], "`") : $column_condition_1);
                    $table_condition_1 = $this->table[(isset($condition[3]) ? $condition[3] : $index_condition)][2] . ".{$column_condition_1}";
                    $table_condition_2 = (isset($condition[4]) ? $this->table[$condition[4]][2] : $alias) . ".{$column_condition_2}";
                    //$table_condition_2 = "{$alias}.{$column_condition_2}";
                    $operator = (isset($condition[1]) ? trim($condition[1]) : "=");
                    $condition = "{$table_condition_1} {$operator} {$table_condition_2}";
                }
                $on = " ON ({$condition})";
            }

            $this->table[$index] = array(true, "{$join} {$database}{$table} {$alias}{$on}", $alias);
            return $this;
        }

        /**
         * Seta a coluna.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $name <p>Coluna</p>
         * @param integer $tablekey <p>Tabela (chave de indicação), padrão = FALSE</p>
         * @param string $alias <p>Apelido para coluna, padrão = FALSE</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function column($name, $tablekey = 1, $alias = false) {
            $table = null;
            if (!$this->is_function($alias)) {
                $index = $this->index("column");
                if (!$this->is_function($name)) {
                    if (!isset($this->table[$tablekey])) {
                        trigger_error("Requested table unreported!");
                        return false;
                    }
                    $column = $this->ereaser($name);
                    if ($name != "*") {
                        //$alias = $column;
                        $table = "{$this->table[$tablekey][2]}.";
                        $column = $table . $this->ereaser($column, "`", false);
                    }
                } else {
                    if ($tablekey) {
                        trigger_error("Unable to determine table in a function!");
                        return false;
                    } else {
                        if (!$alias) {
                            trigger_error("Alias ​​for function not determined!");
                            return false;
                        }
                    }
                    $column = $name;
                    $alias = $this->ereaser($alias);
                }
                $this->columns[$index] = array($column . ($alias ? " AS '{$alias}'" : null), $alias);
                return $this;
            }
            trigger_error("Alias entered is not allowed!");
            return false;
        }

        /**
         * Seta comparação na condição, igual ( = ) ou diferente ( <> ).
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $column <p>Coluna</p>
         * @param string $value <p>Valor</p>
         * @param string $glue <p>Cola para outras condições, padrão = FALSE</p>
         * @param bool $inverse <p>FALSE para igual ou TRUE para diferente, padrão = FALSE</p>
         * @param integer $tablekey <p>Tabela (chave de indicação), padrão = 1</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function match($column, $value, $glue = false, $inverse = false, $tablekey = 1) {
            if (!$this->is_function($column)) {
                if (!isset($this->table[$tablekey])) {
                    trigger_error("Requested table unreported!");
                    return false;
                }
                $column = "{$this->table[$tablekey][2]}." . $this->ereaser($column, "`", false);
            }
            if (!$this->is_function($value)) {
                $value = (strlen($value) > 0 ? "\"" . mysql_real_escape_string($value, $this->link) . "\"" : "NULL");
            }
            $inverse = ($inverse ? "<>" : "=");
            return $this->where(($glue ? "{$glue} " : null) . "{$column} {$inverse} {$value}");
        }

        /**
         * Seta comparação na condição, incluso ( IN ) ou não incluso ( NOT IN ).
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $column <p>Coluna</p>
         * @param string $value <p>Valor</p>
         * @param string $glue <p>Cola para outras condições, padrão = FALSE</p>
         * @param bool $inverse <p>FALSE para incluso ou TRUE para não incluso, padrão = FALSE</p>
         * @param integer $tablekey <p>Tabela (chave de indicação), padrão = 1</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function in($column, $value, $glue = false, $inverse = false, $tablekey = 1) {
            if (is_array($value)) {
                if (!$this->is_function($column)) {
                    if (!isset($this->table[$tablekey])) {
                        trigger_error("Requested table unreported!");
                        return false;
                    }
                    $column = "{$this->table[$tablekey][2]}." . $this->ereaser($column, "`", false);
                }
                foreach ($value as $key => $aux) {
                    if (!$this->is_function($aux)) {
                        $value[$key] = (strlen($aux) > 0 ? "\"" . mysql_real_escape_string($aux, $this->link) . "\"" : "NULL");
                    }
                }
                $value = join(", ", $value);
                $inverse = ($inverse ? "NOT IN" : "IN");
                return $this->where(($glue ? "{$glue} " : null) . "{$column} {$inverse} ({$value})");
            }
            trigger_error("It is necessary to pass the value in array!");
            return false;
        }

        /**
         * Seta comparação na condição, aproximado ( LIKE ) ou não aproximado ( NOT LIKE ).
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $column <p>Coluna</p>
         * @param string $value <p>Valor</p>
         * @param string $glue <p>Cola para outras condições, padrão = FALSE</p>
         * @param bool $inverse <p>FALSE para aproximado ou TRUE para não aproximado, padrão = FALSE</p>
         * @param integer $tablekey <p>Tabela (chave de indicação), padrão = 1</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function like($column, $value, $glue = false, $inverse = false, $tablekey = 1) {
            if (!$this->is_function($column)) {
                if (!isset($this->table[$tablekey])) {
                    trigger_error("Requested table unreported!");
                    return false;
                }
                $column = "{$this->table[$tablekey][2]}." . $this->ereaser($column, "`", false);
            }
            if (!$this->is_function($value)) {
                $value = (strlen($value) > 0 ? "\"" . mysql_real_escape_string($value, $this->link) . "\"" : "NULL");
            }
            $inverse = ($inverse ? "NOT LIKE" : "LIKE");
            return $this->Where(($glue ? "{$glue} " : null) . "{$column} {$inverse} {$value}");
        }

        /**
         * Seta condição manualmente.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $conditions <p>Condição</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function where($conditions) {
            $index = $this->index("where");
            $this->where[$index] = $conditions;
            return $this;
        }

        /**
         * Agrupa colunas ( GROUP BY ).
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param integer $columnkey <p>Coluna (chave de indicação)</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function group($columnkey) {
            if (!isset($this->columns[$columnkey])) {
                trigger_error("Requested column is not declared!");
                return false;
            }
            $index = $this->index("group");
            $this->group[$index] = ($this->columns[$columnkey][1] ? $this->ereaser($this->columns[$columnkey][1], "`") : $this->columns[$columnkey][0]);
            return $this;
        }

        /**
         * Condição do agrupamento de colunas ( HAVING ).
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param integer $columnkey <p>Coluna (chave de indicação)</p>
         * @param string $value <p>Valor</p>
         * @param string $operator <p>Operador, padrão = 1</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function having($columnkey, $value, $operator = "=") {
            if (!$this->is_function($columnkey)) {
                if (!isset($this->columns[$columnkey])) {
                    trigger_error("Requested column is not declared!");
                    return false;
                }
                $column = $this->columns[$columnkey][1];
            }
            if (!$this->is_function($value)) {
                $value = (strlen($value) > 0 ? "\"" . mysql_real_escape_string($value, $this->link) . "\"" : "NULL");
            }
            $index = $this->index("having");
            $this->having[$index] = "{$column} {$operator} {$value}";
            return $this;
        }

        /**
         * Ordena registros por coluna ( ORDER BY ).
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param integer $columnkey <p>Coluna (chave de indicação)</p>
         * @param string $order [ ASC | DESC ] <p>Ordem, padrão = ASC</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function order($columnkey, $order = "ASC") {
            if (!isset($this->columns[$columnkey])) {
                trigger_error("Requested column is not declared!");
                return false;
            }
            $index = $this->index("order");
            $this->order[$index] = ($this->columns[$columnkey][1] ? $this->ereaser($this->columns[$columnkey][1], "`") : $this->columns[$columnkey][0]) . strtoupper(" {$order}");
            return $this;
        }

        /**
         * Limita os registros por consulta ( LIMIT ).
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param integer $limit <p>Total de registros, caso o $length esteja definido esse terá função de ponteiro inicial</p>
         * @param integer $length <p>Total de registros, padrão = FALSE</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function limit($limit, $length = false) {
            $this->limit = $limit . ($length ? ", {$length}" : null);
            return $this;
        }

        /**
         * Executa a operação.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @return array <p>Executando corretamente retorna o array contendo os registros, caso contrário retorna FALSE</p>
         */
        public function go() {
            $table = $columns = $where = $group = $having = $order = $limit = null;
            if ($this->table) {
                foreach ($this->table as $value) {
                    $table .= ( $value[0] ? " " : (is_null($table) ? null : ", ")) . $value[1];
                }
                if ($this->columns) {
                    foreach ($this->columns as $value) {
                        $columns .= ( is_null($columns) ? null : ", ") . $value[0];
                    }
                    if ($this->where) {
                        $where = " WHERE " . join(" ", $this->where);
                    }
                    if ($this->group) {
                        $group = " GROUP BY " . join(", ", $this->group);
                    }
                    if ($this->having) {
                        $having = " HAVING " . join(" AND ", $this->having);
                    }
                    if ($this->order) {
                        $order = " ORDER BY " . join(", ", $this->order);
                    }
                    if ($this->limit) {
                        $limit = " LIMIT " . $this->limit;
                    }
                    $query = "SELECT {$columns} FROM {$table}{$where}{$group}{$having}{$order}{$limit}";
                    $this->result = $this->commit($query);
                    if ($this->result["result"]) {
                        $search = false;
                        if (@mysql_num_rows($this->result["result"]) > 0) {
                            while ($line = mysql_fetch_assoc($this->result["result"])) {
                                $search[] = $line;
                            }
                            mysql_free_result($this->result["result"]);
                        }
                        return $search;
                    }
                    return false;
                }
                trigger_error("Column unreported!");
                return false;
            }
            trigger_error("Table unreported!");
            return false;
        }

        /**
         * Retorna a quantidade de registros afetados.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @return integer <p>Quantidade de registros afetados, caso contrario FALSE</p>
         */
        public function rows() {
            if (isset($this->result["rows"])) {
                return $this->result["rows"];
            }
            return false;
        }

        /**
         * Define ou retorna o ponteiro de um determinado objeto.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $object <p>Objeto</p>
         * @param bool $get <p>TRUE retorna o ponteiro atual do objeto, padrão = FALSE</p>
         * @return integer <p>Ponteiro, caso contrario FALSE</p>
         */
        protected function index($object, $get = false) {
            if ($get) {
                if (isset($this->index[$object])) {
                    return $this->index[$object];
                }
                return false;
            }
            return $this->index[$object] = (isset($this->index[$object]) ? $this->index[$object] + 1 : 1);
        }

    }

}
?>