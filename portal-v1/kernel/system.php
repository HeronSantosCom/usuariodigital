<?php

class System {

    static function Log($message, $filename = "system.log") {
        if (!file_exists(Core::Local("logs"))) {
            mkdir("logs");
        }
        if (!is_array($message)) {
            $message = str_replace("\r\n", "\n", $message);
            $message = str_replace("\n\r", "\n", $message);
            $message = explode("\n", $message);
        }
        $date = date('Y-m-d H:i:s');
        if (isset($_SERVER["REMOTE_ADDR"])) {
            $host = $_SERVER["REMOTE_ADDR"];
            $pid = uniqid();
        } else {
            $host = exec('whoami');
            $pid = getmypid();
        }
        foreach ($message as $line) {
            if (strlen(trim($line)) > 0) {
                $print[] = "[{$date}] [{$host}] [{$pid}] {$line}";
            }
        }
        if (isset($print)) {
            $string = str_replace("´", "\´", join("\n", $print));
            $string = str_replace("`", "\`", $string);
            $string = str_replace('"', '\"', $string);
            exec('echo "' . $string . '" >> ' . Core::Local("logs/{$filename}"));
        }
    }

    static function Get($name, $application = ips_fullname) {
        $application = strtolower($application);
        if (isset($_SESSION['IPS'][$application][$name])) {
            return $_SESSION['IPS'][$application][$name];
        }
        return false;
    }

    static function Set($name, $value = false, $application = ips_fullname) {
        $application = strtolower($application);
        $_SESSION['IPS'][$application][$name] = $value;
        if (!$value) {
            unset($_SESSION['IPS'][$application][$name]);
        }
        return $value;
    }

    static function Extract($array, $prefix = false, $posfix = false, $application = ips_fullname) {
        $application = strtolower($application);
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                self::Set("{$prefix}{$key}{$posfix}", $value, $application);
            }
            return true;
        }
        return false;
    }

    static function Redirect($redirect) {
        header("Location: $redirect");
        exit;
    }

    static function StopLooping($parser) {
        $parser = md5($parser);
        if (self::Get("SLC-{$parser}") > 3) { //Stop Looping Control
            trigger_error("O sistema detectou uma operação ilegal e bloqueou a execução...", E_USER_WARNING);
            return die();
        }
        return self::Set("SLC-{$parser}", (self::Get("SLC-{$parser}") + 1));
    }

    static function Pages($object, $actual, $max = system_interface_limit) {
        $total = $object->where ? $object->Go("OR") : $object->Go();
        if (isset($total[0]["total"])) {
            $pagination = pages($actual, $total[0]["total"], $max);
            if ($pagination) {
                if ($actual != 1) {
                    $array[] = array("page" => 1, "label" => "« Primeira", "title" => "Primeira página...");
                }
                if ($pagination[2]) {
                    $array[] = array("page" => $pagination[2], "label" => "« Anterior", "title" => "Página anterior...");
                }
                $array[] = array("page" => "current", "label" => "{$actual}/{$pagination[4]}", "title" => "Página atual...");
                if ($pagination[3]) {
                    $array[] = array("page" => $pagination[3], "label" => "Próxima »", "title" => "Próxima página...");
                }
                if ($pagination[4] != $actual) {
                    $array[] = array("page" => $pagination[4], "label" => "Última »", "title" => "Última página...");
                }
                return array("{$pagination[0]}, {$pagination[1]}", $array);
            }
        }
        return false;
    }

    static function Clean($application = ips_fullname, $name = false) {
        $application = strtolower($application);
        if ($name) {
            if (isset($_SESSION['IPS'][$application][$name])) {
                unset($_SESSION['IPS'][$application][$name]);
                return true;
            }
        } else {
            unset($_SESSION['IPS'][$application]);
            return true;
        }
        return false;
    }

    static function KeyWords($contents) {
        /* remove "special" characters and words >= 3 characters */
//        $contents = preg_replace('/[^a-z ]*|\s\w{0,3}\s/', '', strtolower($contents));
//
//        /* tokenize the string */
//        $a = false;
//        $tok = strtok($contents, " ");
//        while ($tok !== false) {
//            empty($a[$tok]) ? $a[$tok] = 1 : $a[$tok]++;
//            $tok = strtok(" ");
//        }
//
//        if (is_array($a)) {
//            /* sort */
//            arsort($a);
//
//            /* take the 50 most popular */
//            $a = array_slice($a, 0, (count($a) > 50) ? 50 - count($a) : count($a), true);
//        }
//
//        return $a;
        return false;
    }

    static function MimeType($filename) {
        $pathinfo = pathinfo($filename);
        $extensions = (isset($pathinfo['extension']) ? $pathinfo['extension'] : false);
        switch (strtolower($extensions)) {
            case "xls":
                $mimetype = "application/excel";
                break;
            case "hqx":
                $mimetype = "application/macbinhex40";
                break;
            case "doc":
            case "dot":
            case "wrd":
                $mimetype = "application/msword";
                break;
            case "pdf":
                $mimetype = "application/pdf";
                break;
            case "pgp":
                $mimetype = "application/pgp";
                break;
            case "ps":
            case "eps":
            case "ai":
                $mimetype = "application/postscript";
                break;
            case "ppt":
                $mimetype = "application/powerpoint";
                break;
            case "rtf":
                $mimetype = "application/rtf";
                break;
            case "tgz":
            case "gtar":
                $mimetype = "application/x-gtar";
                break;
            case "gz":
                $mimetype = "application/x-gzip";
                break;
            case "php":
            case "php3":
                $mimetype = "application/x-httpd-php";
                break;
            case "js":
                $mimetype = "application/x-javascript";
                break;
            case "ppd":
            case "psd":
                $mimetype = "application/x-photoshop";
                break;
            case "swf":
            case "swc":
            case "rf":
                $mimetype = "application/x-shockwave-flash";
                break;
            case "tar":
                $mimetype = "application/x-tar";
                break;
            case "zip":
                $mimetype = "application/zip";
                break;
            case "mid":
            case "midi":
            case "kar":
                $mimetype = "audio/midi";
                break;
            case "mp2":
            case "mp3":
            case "mpga":
                $mimetype = "audio/mpeg";
                break;
            case "ra":
                $mimetype = "audio/x-realaudio";
                break;
            case "wav":
                $mimetype = "audio/wav";
                break;
            case "bmp":
                $mimetype = "image/bitmap";
                break;
            case "gif":
                $mimetype = "image/gif";
                break;
            case "iff":
                $mimetype = "image/iff";
                break;
            case "jb2":
                $mimetype = "image/jb2";
                break;
            case "jpg":
            case "jpe":
            case "jpeg":
                $mimetype = "image/jpeg";
                break;
            case "jpx":
                $mimetype = "image/jpx";
                break;
            case "png":
                $mimetype = "image/png";
                break;
            case "tif":
            case "tiff":
                $mimetype = "image/tiff";
                break;
            case "wbmp":
                $mimetype = "image/vnd.wap.wbmp";
                break;
            case "xbm":
                $mimetype = "image/xbm";
                break;
            case "css":
                $mimetype = "text/css";
                break;
            case "txt":
                $mimetype = "text/plain";
                break;
            case "htm":
            case "html":
                $mimetype = "text/html";
                break;
            case "xml":
                $mimetype = "text/xml";
                break;
            case "mpg":
            case "mpe":
            case "mpeg":
                $mimetype = "video/mpeg";
                break;
            case "qt":
            case "mov":
                $mimetype = "video/quicktime";
                break;
            case "avi":
                $mimetype = "video/x-ms-video";
                break;
            case "eml":
                $mimetype = "message/rfc822";
                break;
            default:
                $mimetype = (function_exists("mime_content_type") ? mime_content_type($filename) : "application/octet-stream");
                break;
        }
        return $mimetype;
    }

}

?>
