<?php

/**
 * Requisição de ponteiro de diretório atual / sistema
 */
class Core extends System {

    /**
     * Redefine os ponteiros de diretório do Core
     */
    static function Explore() {
        if (!isset($_ENV["IPS"]["LOCATION"])) {
            $_ENV["IPS"]["LOCATION"] = false;
            $dirpath = pathinfo(__file__);
            $dirpath = str_replace("\\", "/", $dirpath["dirname"]);
            $dirlocal = explode('/kernel', $dirpath);
            $_ENV["IPS"]["LOCATION"]["LOCAL"] = $dirlocal[0];
            $dirlocal = explode('/', $dirlocal[0]);
            $dirname = array_pop($dirlocal);
            $_ENV["IPS"]["LOCATION"]["DIRECTORY"] = "/{$dirname}";
            $dirpath = explode("/{$dirname}", $_ENV["IPS"]["LOCATION"]["LOCAL"]);
            $_ENV["IPS"]["LOCATION"]["PATH"] = $dirpath[0];
            if (isset($_SERVER['REQUEST_URI'])) {
                $diruri = null;
                if (preg_match("#{$_ENV["IPS"]["LOCATION"]["DIRECTORY"]}#", $_SERVER['REQUEST_URI'])) {
                    $diruri = explode($_ENV["IPS"]["LOCATION"]["DIRECTORY"], $_SERVER['REQUEST_URI'], 2); //extrai o caminho remoto
                    $diruri = $diruri[0] . $_ENV["IPS"]["LOCATION"]["DIRECTORY"];
                }
                $_ENV["IPS"]["LOCATION"]["REMOTE"] = $diruri;
                $httppath = "http://127.0.0.1";
                if (isset($_SERVER["HTTP_HOST"]) and strlen($_SERVER["HTTP_HOST"]) > 0) {
                    $httpprefix = "http://";
                    if (isset($_SERVER["HTTPS"])) {
                        $httpprefix = "https://";
                    }
                    $httppath = $httpprefix . $_SERVER["HTTP_HOST"];
                }
                $_ENV["IPS"]["LOCATION"]["URI"] = $httppath . $diruri;

                $_ENV["IPS"]["LOCATION"]["PERMALINK"] = false;
                $directory = $_ENV["IPS"]["LOCATION"]["DIRECTORY"];
                $starter = null;
                if (preg_match("#$directory#", $_SERVER['REQUEST_URI'])) {
                    $dir = explode($directory, $_SERVER['REQUEST_URI'], 2);
                    $starter = "{$dir[0]}{$directory}";
                }
                $starter .= "/";
                $permalink = explode($starter, $_SERVER['REQUEST_URI'], 2);
                if (isset($permalink[1])) {
                    $permalink = explode("?", $permalink[1], 2);
                    $permalink = explode("#", $permalink[0], 2);
                    $permalink = $permalink[0];
                    if (substr($permalink, strlen($permalink) - 1) == "/") {
                        $permalink = substr($permalink, 0, strlen($permalink) - 1);
                    }
                    $permalink = explode("/", $permalink);
                    if (count($permalink) == 1 and strlen($permalink[0]) == 0) {
                        $permalink = false;
                    }
                    if (is_array($permalink)) {
                        $_ENV["IPS"]["LOCATION"]["OPENED"] = join("/", $permalink);
                        foreach ($permalink as $column => $value) {
                            $_ENV["IPS"]["LOCATION"]["PERMALINK"][$column] = $value;
                        }
                    }
                }
            }
            if (isset($_SERVER["argv"])) {
                $permalink = $_SERVER["argv"];
                array_shift($permalink);
                if (is_array($permalink) and count($permalink) > 0) {
                    foreach ($permalink as $column => $value) {
                        $_ENV["IPS"]["LOCATION"]["PERMALINK"][$column] = $value;
                    }
                }
            }
        }
    }

    /**
     * Retorna o caminho local da aplicação no servidor
     * @param string [optional] $sufix <p>caminho a ser acrescido se o /</p>
     * @return string
     */
    static function Local($sufix = null) {
        self::Explore();
        if (isset($_ENV["IPS"]["LOCATION"]["LOCAL"])) {
            return $_ENV["IPS"]["LOCATION"]["LOCAL"] . ($sufix ? "/{$sufix}" : null);
        }
        return ($sufix ? $sufix : false);
    }

    /**
     * Retorna o diretorio (start/root) da aplicação no servidor
     * @param string [optional] $sufix <p>caminho a ser acrescido se o /</p>
     * @return string
     */
    static function Directory($sufix = null) {
        self::Explore();
        if (isset($_ENV["IPS"]["LOCATION"]["DIRECTORY"])) {
            return $_ENV["IPS"]["LOCATION"]["DIRECTORY"] . ($sufix ? "/{$sufix}" : null);
        }
        return ($sufix ? $sufix : false);
    }

    /**
     * Retorna a raiz aonde encontra-se a aplicação no servidor
     * @param string [optional] $sufix <p>caminho a ser acrescido se o /</p>
     * @return string
     */
    static function Path($sufix = null) {
        self::Explore();
        if (isset($_ENV["IPS"]["LOCATION"]["PATH"])) {
            return $_ENV["IPS"]["LOCATION"]["PATH"] . ($sufix ? "/{$sufix}" : null);
        }
        return ($sufix ? $sufix : false);
    }

    /**
     * Retorna o diretorio virtual (sem o host) da aplicação
     * @param string [optional] $sufix <p>caminho a ser acrescido</p>
     * @return string
     */
    static function Remote($sufix = null) {
        self::Explore();
        if (isset($_ENV["IPS"]["LOCATION"]["REMOTE"])) {
            return $_ENV["IPS"]["LOCATION"]["REMOTE"] . ($sufix ? "/{$sufix}" : null);
        }
        return ($sufix ? $sufix : false);
    }

    /**
     * Retorna o caminho virtual (URL/URI) da aplicação
     * @param string [optional] $sufix <p>caminho a ser acrescido</p>
     * @return string
     */
    static function Uri($sufix = null) {
        self::Explore();
        if (isset($_ENV["IPS"]["LOCATION"]["URI"])) {
            return $_ENV["IPS"]["LOCATION"]["URI"] . ($sufix ? "/{$sufix}" : null);
        }
        return ($sufix ? $sufix : false);
    }

    static function Opened($sufix = null) {
        self::Explore();
        if (isset($_ENV["IPS"]["LOCATION"]["OPENED"])) {
            return $_ENV["IPS"]["LOCATION"]["OPENED"] . ($sufix ? "/{$sufix}" : null);
        }
        return ($sufix ? $sufix : false);
    }

    static function Referrer($first = false) {
        $first = ($first ? $first : self::Opened());
        $remote = self::Opened();
        if ($remote == $first) {
            unset($_SESSION["IPS-REFERRER"]);
        }
        $_SESSION["IPS-REFERRER"][] = $remote;
        $referrer = $first;
        if (isset($_SESSION["IPS-REFERRER"])) {
            $parser = $_SESSION["IPS-REFERRER"];
            unset($_SESSION["IPS-REFERRER"]);
            foreach ($parser as $key => $value) {
                $_SESSION["IPS-REFERRER"][] = $value;
                if ($value == $remote) {
                    break;
                }
                $referrer = $value;
            }
        }
        return $referrer;
    }

    /**
     * Carrega internamente uma página externa
     * @param string $uri <p>caminho a ser carregado</p>
     * @param string [optional] $method <p>metodo de carregamento<ul><li>fopen</li><li>curl</li></ul></p>
     * @return string
     */
    static function External($uri, $method = "curl") {
        $contents = false;
        switch (strtolower($method)) {
            case "fopen":
                ini_set("user_agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");
                $pointer = @fopen($uri, "r");
                if ($pointer) {
                    $contents = null;
                    while (!feof($pointer)) {
                        $contents .= @ fgets($pointer, 4096);
                    }
                    fclose($pointer);
                }
                break;
            case "curl":
            default:
                $pointer = curl_init($uri);
                curl_setopt($pointer, CURLOPT_TIMEOUT, 15);
                curl_setopt($pointer, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");
                curl_setopt($pointer, CURLOPT_FOLLOWLOCATION, true);
                ob_start();
                curl_exec($pointer);
                curl_close($pointer);
                $contents = ob_get_contents();
                ob_end_clean();
                break;
        }
        if (eregi('301 Moved Permanently', $contents)) {
            preg_match("/HREF=\"(.*)\"/si", $contents, $pointer);
            return self::External($pointer[1], $method);
        }
        return $contents;
    }

}

?>
