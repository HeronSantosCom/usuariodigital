<?php

/**
 * Carrega classes dependentes
 */
if (!class_exists("MySQLSave", false)) {
    include 'kernel/mysql/save.php';
}

if (!class_exists("MySQLDelete", false)) {
    include 'kernel/mysql/delete.php';
}

if (!class_exists("MySQLSearch", false)) {
    include 'kernel/mysql/search.php';
}

if (!class_exists("MySQLFormat", false)) {
    include 'kernel/mysql/format.php';
}

if (!class_exists("MySQLShow", false)) {
    include 'kernel/mysql/show.php';
}

/**
 * Classe de manipulação de Banco de Dados MySQL
 */
class MySQL extends System {

    /**
     * Variáveis requeridas
     */
    protected $server, $username, $password, $database, $maxlimit, $timezone, $encoding;
    private $query = false;
    public $link, $global;

    /**
     *
     * @param string $server
     * @param string $username
     * @param string $password
     * @param string $database
     * @param int [optional] $maxlimit
     * @param bool [optional] $global
     * @param string [optional] $timezone
     * @param string [optional] $encoding
     * @param bool [optional] $debug
     */
    public function __construct($server, $username, $password, $database, $maxlimit = 3000, $global = false, $timezone = false, $encoding = false) {
        $this->server = $server;
        $this->username = $username;
        $this->password = $password;
        $this->database = $database;
        $this->maxlimit = $maxlimit;
        $this->global = $global;
        $this->timezone = $timezone;
        $this->encoding = $encoding;
        $this->Connect();
    }

//    /**
//     * Armazenas as informaçõs a nível de sistema
//     * @return array
//     */
//    public function __sleep() {
//        return array('server', 'username', 'password', 'database', 'maxlimit', 'global', 'timezone', 'encoding');
//    }
//
//    /**
//     * Restabelece a conexão em case de descanço
//     */
//    public function __wakeup() {
//        $this->Connect();
//    }
//
    /**
     * Destroe o link Global
     */
    public function __destruct() {

    }

    /**
     * Efetua conexão e cria uma nova instancia
     * @return bool
     */
    public function Connect() {
        if ($this->global) {
            if (self::GetInstance()) {
                parent::Log("Using global instance...", "mysql.log");
                return true;
            }
        }
        $this->link = mysql_connect($this->server, $this->username, $this->password);
        parent::Log("Connected at {$this->username}@{$this->server}...", "mysql.log");
        if ($this->link) {
            if (!mysql_select_db($this->database, $this->link)) {
                trigger_error("Não foi possível selecionar o banco de dados {$this->database}.", E_USER_WARNING);
                return false;
            }
            parent::Log("Specifying database {$this->database}...", "mysql.log");
            if ($this->encoding) {
                if (mysql_client_encoding() != $this->encoding) {
                    mysql_query("SET NAMES {$this->encoding}", $this->link);
                    parent::Log("Specifying enconding {$this->encoding}...", "mysql.log");
                }
            }
            if ($this->timezone) {
                parent::Log("Specifying time zone {$this->timezone}...", "mysql.log");
                mysql_query("SET SESSION time_zone = '{$this->timezone}'", $this->link);
            }
            if ($this->global) {
                parent::Log("Configuring global instance {$this->username}@{$this->server}/{$this->database}...", "mysql.log");
                self::SetInstance($this);
            }
            return true;
        }
        trigger_error("Não foi possível conectar ao servidor de banco de dados {$this->server}.", E_USER_WARNING);
        return false;
    }

    public function Disconnect() {
        if (is_resource($this->link)) {
            if (mysql_close($this->link)) {
                parent::Log("Disconnected from {$this->username}@{$this->server}", "mysql.log");
                return true;
            }
        }
        return false;
    }

    /**
     * Armazena as sql numa fila
     * @param string $query
     * @return MySQL
     */
    public function Queue($query) {
        $this->query[] = $query;
        return $this;
    }

    /**
     * Executa as querys armazenadas na fila ou a sql requisitada pelo $query
     * @param string $query
     * @return array
     */
    public function Commit($query = false) {
        if (!$query) {
            if (!is_array($this->query)) {
                trigger_error("Nenhum comando a ser executado!", E_USER_WARNING);
                return false;
            }
            $query = join("; ", $this->query);
            $this->query = false;
        }
        $result["result"] = mysql_query($query, $this->link);
        parent::Log("Query: {$query}", "mysql.log");
        $result["rows"] = mysql_affected_rows($this->link);
        parent::Log("Rowns: {$result["rows"]}", "mysql.log");
        $result["id"] = mysql_insert_id($this->link);
        parent::Log("Id: {$result["id"]}", "mysql.log");
        $result["error"] = mysql_error($this->link);
        parent::Log("Error: {$result["error"]}", "mysql.log");
        return $result;
    }

    /**
     * Retorna a chave primária de uma tabela
     * @param string $table
     * @return string
     */
    public function PrimaryKey($table) {
        $return = $this->Commit("SHOW /*!32332 FULL */ COLUMNS FROM `{$table}` WHERE `KEY` = 'PRI'");
        if ($return["result"]) {
            if (mysql_num_rows($return["result"]) > 0) {
                return mysql_result($return["result"], 0, 'Field');
            }
        }
        return false;
    }

    /**
     * Retorna a instancia de uma conexão global
     * @param object $instance
     * @return mixed
     */
    static function GetInstance($instance = false) {
        if (!$instance) {
            $instance = parent::Get("MGL-Instance");
        }
        if (isset($instance->link)) {
            if (is_resource($instance->link)) {
                return $instance;
            }
        }
        return false;
    }

    /**
     * Define a instancia de uma conexão global
     * @param object $instance
     * @return bool
     */
    static function SetInstance($instance) {
        if (isset($instance->link)) {
            if (is_resource($instance->link)) {
                parent::Set("MGL-Instance", $instance);
                return true;
            }
        }
        return false;
    }

}

?>
