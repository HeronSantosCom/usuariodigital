<?php

class postagens extends control {

    public function __construct($id = false) {
        if ($id) {
            $postagens = new mysqlsearch();
            $postagens->table("postagens_view");
            $postagens->column("*");
            $postagens->match("id", $id);
            $postagens = $postagens->go();
            if (isset($postagens[0])) {
                $this->extract($postagens[0]);
            }
        }
    }
    
    public function editar () {
        $this->fontes = fontes::listagem();
    }

    public function salvar($id = false) {
        
    }

}

?>
