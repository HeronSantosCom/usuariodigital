<?php

class SMTP {

    private $from, $to, $cc, $bcc, $mail, $attach;
    protected $server, $port, $ssl, $auth, $timeout, $charset;
    public $socket;

    public function __construct($server, $port = 25, $ssl = false, $auth = false, $timeout = 45, $charset = false) {
        $this->server = ($ssl ? "ssl://{$server}:{$port}" : $server);
        $this->port = ($ssl ? 25 : $port);
        $this->host = $server;
        $this->ssl = $ssl;
        $this->auth = $auth;
        $this->timeout = $timeout;
        $this->charset = $charset;
    }

    public function __sleep() {
        return array('server', 'port', 'ssl', 'auth', 'timeout', 'charset');
    }

    public function __wakeup() {
        $this->Connect($this->auth);
    }

    public function __destruct() {
        $this->Disconnect();
    }

    public function Connect($user = false, $password = false) {
        $this->socket = new Socket($this->server, $this->port, $this->timeout);
        if ($this->socket->Open()) {
            if ($this->Code($this->socket->Get()) == '220') {
                if ($this->auth) {
                    if ($this->Code($this->socket->Get("EHLO {$this->host}")) == '250') {
                        if ($this->Code($this->socket->Get("RSET")) == '250') {
                            if ($this->Code($this->socket->Get("AUTH LOGIN")) == '334') {
                                if ($this->Code($this->socket->Get(base64_encode($user))) == '334') {
                                    if ($this->Code($this->socket->Get(base64_encode($password))) == '235') {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                if ($this->Code($this->socket->Get("HELO {$this->host}")) == '250') {
                    if ($this->Code($this->socket->Get("RSET")) == '250') {
                        return true;
                    }
                }
            }
            $this->Disconnect();
        }
        return true;
    }

    public function SendFrom($name, $email) {
        if (!$this->Block($email)) {
            $this->from = array($name, $email);
        }
        return $this;
    }

    public function SendTo($name, $email) {
        if (!$this->Block($email)) {
            $this->to[] = array($name, $email);
        }
        return $this;
    }

    public function SendCC($name, $email) {
        trigger_error('Aviso do desenvolvedor: A função SendCC da classe SMTP não está funcionando corretamente, utilize SendTo temporariamente!', E_USER_NOTICE);
        if (!$this->Block($email)) {
            $this->cc[] = array($name, $email);
        }
        return $this;
    }

    public function SendBCC($name, $email) {
        trigger_error('Aviso do desenvolvedor: A função SendBCC da classe SMTP não está funcionando corretamente!', E_USER_NOTICE);
        if (!$this->Block($email)) {
            $this->bcc[] = array($name, $email);
        }
        return $this;
    }

    public function Attach($filename, $description) {
        if (file_exists($filename)) {
            $pathinfo = pathinfo($filename);
            $basename = $pathinfo['basename'];
            $extension = $pathinfo['extension'];
            $mimetype = System::MimeType($filename);
            $this->attach[] = array($filename, $mimetype, $basename, $extension, $description);
        }
        return $this;
    }

    public function Mail($subject, $content, $type = "text/plain", $priority = false) {
        if ($this->socket) {
            if ($this->from and $this->to) {
                $from = "\"{$this->from[0]}\" <{$this->from[1]}>";
                $cc = false;
                if ($this->cc) {
                    foreach ($this->cc as $email) {
                        $cc[] = "\"{$email[0]}\" <{$email[1]}>";
                    }
                    $cc = join(", ", $cc);
                }
                $bcc = false;
                if ($this->bcc) {
                    foreach ($this->bcc as $email) {
                        $bcc[] = "\"{$email[0]}\" <{$email[1]}>";
                    }
                    $bcc = join(", ", $bcc);
                }
                $response = false;
                foreach ($this->to as $key => $email) {
                    $response[$key] = false;
                    $to = "\"{$email[0]}\" <{$email[1]}>";
                    if ($this->Code($this->socket->Get("MAIL FROM: <{$this->from[1]}>")) == '250') {
                        if ($this->Code($this->socket->Get("RCPT TO: <{$email[1]}>")) == '250') {
                            if ($this->Code($this->socket->Get("DATA")) == '354') {
                                $id = date('YmdHis') . '.' . md5(microtime()) . '.' . strtoupper($email[1]);
                                $boundary = md5(uniqid(time()));
                                $date = date("r");
                                $this->socket->Put("MIME-Version: 1.0");
                                $this->socket->Put("Reply-To: {$this->from[1]}");
                                $this->socket->Put("Date: {$date}");
                                $this->socket->Put("Delivered-To: {$this->from[1]}");
                                $this->socket->Put("Message-ID: <{$id}>");
                                $this->socket->Put("Subject: {$subject}");
                                $this->socket->Put("From: {$from}");
                                $this->socket->Put("To: {$to}");
                                if ($cc) {
                                    $this->socket->Put("Cc: {$cc}");
                                }
                                if ($bcc) {
                                    $this->socket->Put("Bcc: {$bcc}");
                                }
                                if ($priority) {
                                    $this->socket->Put("X-Mailer: " . ips_fullname);
                                    $this->socket->Put("X-MSMail-Priority: {$priority}");
                                }
                                if ($this->attach) {
                                    $this->socket->Put("Content-type: multipart/mixed; boundary={$boundary}");
                                    $this->socket->Put("\r\n");
                                    $this->socket->Put("--" . $boundary);
                                }
                                $this->socket->Put("Content-Type: {$type}; charset={$this->charset}");
//                                $this->socket->Put('Content-Transfer-Encoding: 8bit');
                                $this->socket->Put("\r\n");
                                $this->socket->Put($content);
                                //$this->socket->Put("\r\n");
                                if ($this->attach) {
                                    $this->socket->Put("\r\n");
                                    foreach ($this->attach as $file) {
                                        $id = date('YmdHis') . '.' . md5(microtime()) . '.' . strtoupper($email[1]);
                                        $this->socket->Put("--{$boundary}");
                                        $this->socket->Put("Content-Type: {$file[1]}; name=\"{$file[2]}\"");
                                        $this->socket->Put("Content-Disposition: attachment; filename=\"{$file[2]}\"");
                                        $this->socket->Put("Content-Transfer-Encoding: base64");
                                        $this->socket->Put("X-Attachment-Id: {$id}");
                                        $this->socket->Put("\r\n");
                                        $this->socket->Put(chunk_split(base64_encode(file_get_contents($file[0]))));
                                        $this->socket->Put('--' . $boundary . '--');
                                        //$this->socket->Put("\r\n");
                                    }
                                }
                                //if ($this->Code($this->socket->Get("\r\n\r\n.")) == '250') {
                                if ($this->Code($this->socket->Get(".")) == '250') {
                                    $response[$key] = true;
                                }
                            }
                        }
                    }
                }
                return $response;
            }
        }
        return false;
    }

    public function Disconnect() {
        if ($this->socket) {
            if ($this->socket->Close()) {
                $this->socket = false;
                return true;
            }
        }
        return true;
    }

    private function Code($value, $length = 3) {
        if (is_array($value)) {
            $value = join("\n", $value);
        }
        if (strlen($value) > ($length - 1)) {
            return substr(trim($value), 0, $length);
        }
        return false;
    }

    private function Block($email) {
        if (preg_match("#(@127.0.0.1|@localhost|@localdomain)#", $email)) {
            return true;
        }
        return false;
    }

}

?>
