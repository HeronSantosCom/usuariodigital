<?php

if (!class_exists('mysql')) {

    /**
     * Responsável pela conexão com bases de dados MySQL.
     * @name kengoo/kernel/mysql
     * @author Heron Santos
     * @copyright www.heronsantos.com
     */
    class mysql {

        protected $server, $username, $password, $select, $maxlimit, $timezone, $encoding, $link;
        private $query = false;

        public function __construct() {
            $this->server = mysql_server;
            $this->username = mysql_username;
            $this->password = mysql_password;
            $this->select = mysql_database;
            $this->maxlimit = mysql_limit;
            $this->timezone = system_timezone_unix;
            $this->encoding = system_encoding;
            $this->connect();
        }

        /**
         * Conecta com a base de dados ou restaura conexão.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @return bool <p>TRUE ou FALSE caso não consiga conectar ou restaurar conexão</p>
         */
        public function connect() {
            if (!is_resource($this->link)) {
                $this->link = mysql_connect($this->server, $this->username, $this->password);
                if ($this->link) {
                    if (mysql_select_db($this->select, $this->link)) {
                        if ($this->encoding) {
                            if (mysql_client_encoding() != $this->encoding) {
                                mysql_query("SET NAMES {$this->encoding}", $this->link);
                            }
                        }
                        if ($this->timezone) {
                            mysql_query("SET SESSION time_zone = '{$this->timezone}'", $this->link);
                        }
                        return true;
                    }
                    trigger_error("Unable to select database {$this->select} in {$this->username}@{$this->server}.");
                    return false;
                }
            }
            trigger_error("Could not connect to database server {$this->username}@{$this->server}.");
            return false;
        }

        /**
         * Desconecta com a base de dados.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @return bool <p>TRUE ou FALSE caso não consiga desconectar conexão</p>
         */
        public function disconnect() {
            if (is_resource($this->link)) {
                if (mysql_close($this->link)) {
                    kernel::log("Disconnected from {$this->username}@{$this->server}", "mysql.log");
                    return true;
                }
            }
            return false;
        }

        /**
         * Armazenas as consultas numa fila em buffer.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $query <p>Consulta a ser executada</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function queue($query) {
            $this->query[] = $query;
            return $this;
        }

        /**
         * Executa as consultas armazenadas em buffer ou expressadas no parametros $query.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $query <p>Consulta a ser executada</p>
         * @return array <p>Contém o resultado(result), linhas afetadas(rows), identificador ou indice(id), descrição do erro(erro)</p>
         */
        public function commit($query = false) {
            if (!$query) {
                if (!is_array($this->query)) {
                    trigger_error("No command to run!");
                    return false;
                }
                $query = join("; ", $this->query);
                $this->query = false;
            }
            $result["result"] = mysql_query($query, $this->link);
            $result["rows"] = mysql_affected_rows($this->link);
            $result["id"] = mysql_insert_id($this->link);
            $result["error"] = mysql_error($this->link);
            kernel::log("Query: {$query}", "mysql.log");
            kernel::log("Rowns: {$result["rows"]}", "mysql.log");
            kernel::log("Id: {$result["id"]}", "mysql.log");
            kernel::log("Error: {$result["error"]}", "mysql.log");
            return $result;
        }

        /**
         * Retorna qual a chave primária da tabela solicitada.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $table <p>Tabela</p>
         * @return string <p>Chave primária da tabela, caso contrario FALSE</p>
         */
        public function primarykey($table) {
            $return = $this->commit("SHOW /*!32332 FULL */ COLUMNS FROM `{$table}` WHERE `KEY` = 'PRI'");
            if ($return["result"]) {
                if (mysql_num_rows($return["result"]) > 0) {
                    return mysql_result($return["result"], 0, 'Field');
                }
            }
            return false;
        }

        /**
         * Limpa o valor.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $value <p>Valor</p>
         * @param string $capsulate <p>Encapsulador, padrão = FALSE</p>
         * @param bool $exception <p>Excessão caso o $value conter uma função ele não reportar erro, padrão = FALSE</p>
         * @return string <p>Valor limpo</p>
         */
        protected function ereaser($value, $capsulate = false, $exception = false) {
            $function = self::is_function($value);
            if ($function) {
                if ($capsulate and !$exception) {
                    trigger_error("You can not encapsulate the function {$function}.");
                    return false;
                }
            } else {
                $value = trim(str_replace(array('"', "'", "`"), "", $value));
                if (strlen($value) > 0 and $capsulate) {
                    $value = "{$capsulate}{$value}{$capsulate}";
                }
            }
            return $value;
        }

        /**
         * Detecta se é função.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $value <p>Valor</p>
         * @return string <p>Retorna qual é a função sendo executada, caso contrario FALSE</p>
         */
        protected function is_function($value) {
            $position = trim(strpos($value, "("));
            if ($position > 0) {
                $function = strtoupper(trim(substr($value, 0, $position)));
                switch ($function) {
                    case "COUNT":
                    case "AVG":
                    case "CONCAT":
                    case "CONCAT_WS":
                    case "IF":
                    case "MAX":
                    case "MIN":
                    case "MD5":
                    case "DATE":
                    case "DATE_FORMAT":
                    case "LENGTH":
                        return $function;
                        break;
                }
            }
            return false;
        }

    }

}
?>