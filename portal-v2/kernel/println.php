<?php

if (!function_exists("println")) {

    /**
     * Imprime uma linha acrescentando uma quebra de linha.
     * @author Heron Santos
     * @copyright www.heronsantos.com
     * @param string $string <p>Valor</p>
     * @return void
     */
    function println($string) {
        echo $string . "\n";
        flush();
    }

}
?>