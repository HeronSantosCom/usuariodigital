<?php

class Source extends System {

    static function Explore($source) {
        parent::StopLooping($source);
        if (preg_match('/{%*([^%}]+?)(\1|%})/i', $source, $struction)) {
            if (isset($struction[1])) {
                if (preg_match('/(.*?)[\(](.*)[\)](.*)\s/i', $struction[1], $function)) {
                    if (isset($function[2])) {
                        $decode = false;
                        switch (trim($function[1])) {
                            case "application":
                                $value = null;
                                $class = trim($function[2]);
                                if (!parent::Get("OSOC-{$class}")) {
                                    parent::Set("OSOC-{$class}", new $class());
                                }
                                break;
                            case "run":
                                $class = explode(">", trim($function[2]));
                                if (isset($class[1])) {
                                    $static = $class[1];
                                    $class = $class[0];
                                    if (!parent::Get("OSOC-{$class}")) {
                                        $parser = new $class();
                                        parent::Set("OSOC-{$class}", $parser);
                                    } else {
                                        $parser = parent::Get("OSOC-{$class}");
                                    }
                                    $parser->$static();
                                } else {
                                    $class = $class[0];
                                    if (!parent::Get("OSOC-{$class}")) {
                                        new $class();
                                    }
                                }
                                break;
                            case "interface":
                            case "show":
                                $type = false;
                                if (isset($function[3]) and strlen(trim($function[3])) > 0) {
                                    $type = trim($function[3]);
                                }
                                $value = Interfaces::Show(trim($function[2]), $type, true);
                                break;
                            case "ajax":
                                $value = null;
                                $path = (trim($function[2]) == "true" ? true : false);
                                if ($path) {
                                    parent::Set("OIS-Ajax", true);
                                }
                                break;
                            case "set":
                                $value = null;
                                $path = trim($function[2]);
                                if (isset($function[3]) and strlen(trim($function[3])) > 0) {
                                    parent::Set(trim($function[3]), "$path");
                                }
                            case "html":
                                $decode = true;
                            case "get":
                                if (isset($function[3]) and strlen(trim($function[3])) > 0) {
                                    $value = parent::Get(trim($function[2]), trim($function[3]));
                                } else {
                                    $value = parent::Get(trim($function[2]));
                                }
                                if (is_array($value)) {
                                    $value = "1";
                                }
                                if (!$decode) {
                                    $value = htmlentities($value, ENT_COMPAT, system_interface_encoding);
                                }
                                //$value = htmlspecialchars($value, ENT_COMPAT, system_interface_encoding);
                                break;
                            case "constant":
                                $value = constant(trim($function[2]));
                                if (is_array($value)) {
                                    $value = "1";
                                }
                                break;
                            case "url":
                                $path = trim($function[2]);
                                $path = ($path == "this" ? $_SERVER['REQUEST_URI'] : Core::Uri($path));
                                if (isset($function[3])) {
                                    if (trim($function[3]) == "cache") {
                                        $path .= ( strpos($path, "?") ? "&" : "?") . time();
                                    }
                                }
                                $value = $path;
                                break;
                            case "uri":
                                $path = trim($function[2]);
                                $path = ($path == "this" ? $_SERVER['REQUEST_URI'] : Core::Remote($path));
                                if (isset($function[3])) {
                                    if (trim($function[3]) == "cache") {
                                        $path .= ( strpos($path, "?") ? "&" : "?") . time();
                                    }
                                }
                                $value = $path;
                                break;
                            case "if":
                                $source = self::So($function, $source);
                                break;
                            case "for":
                                $source = self::To($function, $source);
                                break;
                            case "each":
                                $source = self::Each($function, $source);
                                break;
                        }
                    }
                }
                $source = self::Explore(str_replace($struction[0], (isset($value) ? $value : false), $source));
            }
        }
        return $source;
    }

    static function To($condition, $haystack) {
        $function = $condition[0];
        $name = trim($condition[3]);
        $condition = trim($condition[2]);
        $init = strpos($haystack, "{%{$function}%}");
        $parser = substr($haystack, $init);
        $end = strpos($parser, "{% end({$name}) %}") + strlen("{% end({$name}) %}");
        $parser = substr($parser, 0, $end);
        $offset = substr($parser, strlen("{%{$function}%}"), -strlen("{% end({$name}) %}"));
        if (preg_match('/(.*),(.*)/i', $condition, $function)) {
            if (isset($function[2])) {
                $start = trim($function[1]);
                $end = trim($function[2]);
                if ($end >= $start) {
                    for ($x = $start; $x <= $end; $x++) {
                        $replaced = str_replace('$' . $name . '()', $x, $offset);
                        $replace[] = $replaced;
                    }
                }
                $haystack = str_replace($parser, (isset($replace) ? join("", $replace) : null), $haystack);
            }
        }
        return $haystack;
    }

    static function Each($condition, $haystack) {
        $function = $condition[0];
        $name = trim($condition[3]);
        $condition = trim($condition[2]);
        $init = strpos($haystack, "{%{$function}%}");
        $parser = substr($haystack, $init);
        $end = strpos($parser, "{% end({$name}) %}") + strlen("{% end({$name}) %}");
        $parser = substr($parser, 0, $end);
        $offset = substr($parser, strlen("{%{$function}%}"), -strlen("{% end({$name}) %}"));
        if (preg_match('/(.*)/i', $condition, $function)) {
            if (isset($function[1])) {
                $array = parent::Get(trim($function[1]));
                if (is_array($array)) {
                    foreach ($array as $key => $value) {
                        $replaced = str_replace('$' . $name . '()', $key, $offset);
                        if (is_array($value)) {
                            foreach ($value as $field => $val) {
                                $val = htmlentities($val, ENT_COMPAT, system_interface_encoding);
                                $replaced = str_replace('$' . $name . '(' . $field . ')', (is_array($val) ? null : $val), $replaced);
                            }
                        } else {
                            $value = htmlentities($value, ENT_COMPAT, system_interface_encoding);
                            $replaced = str_replace('$' . $name . '(this)', $value, $replaced);
                        }
                        $replace[] = $replaced;
                    }
                }
                $haystack = str_replace($parser, (isset($replace) ? join("", $replace) : null), $haystack);
            }
        }
        return $haystack;
    }

    static function So($condition, $haystack) {
        $function = $condition[0];
        $name = trim($condition[3]);
        $condition = trim($condition[2]);
        $init = strpos($haystack, "{%{$function}%}");
        $parser = substr($haystack, $init);
        $else = strpos($parser, "{% else({$name}) %}");
        $end = strpos($parser, "{% end({$name}) %}");
        $parser = substr($parser, 0, $end + strlen("{% end({$name}) %}"));
        $parser_1 = substr($parser, strlen("{%{$function}%}"), $end);
        $parser_2 = null;
        if (strlen($else) > 0) {
            $parser_1 = substr($parser, strlen("{%{$function}%}"), $else - strlen("{%{$function}%}"));
            $parser_2 = substr($parser, $else + strlen("{% else({$name}) %}"), $end - ($else + strlen("{% else({$name}) %}")));
        }
        if (preg_match('/(.*)(.[\!\=|\=\=|\>|\>\=|\<|&&|\<\=])(.*)/i', $condition, $function)) {
            if (isset($function[3])) {
                $x = trim($function[1]);
                $decision = trim($function[2]);
                $y = trim($function[3]);
                $offset = false;
                switch ($decision) {
                    case '==':
                        if ($x == $y) {
                            $offset = true;
                        }
                        break;
                    case '&&':
                        if (preg_match("#($y)#", $x)) {
                            $offset = true;
                        }
                        break;
                    case '!=':
                        if ($x != $y) {
                            $offset = true;
                        }
                        break;
                    case '>=':
                        if ((double) $x >= (double) $y) {
                            $offset = true;
                        }
                        break;
                    case '<=':
                        if ((double) $x <= (double) $y) {
                            $offset = true;
                        }
                        break;
                }
                $haystack = str_replace($parser, ($offset ? $parser_1 : $parser_2), $haystack);
            }
        }
        return $haystack;
    }

}

?>
