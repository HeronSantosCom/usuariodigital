<?php

class Category {

    private $ids, $search, $options;
    private $page = 1;

    public function __construct() {
        Index::MySQL();
        System::Set("Title", "Categorias");
        if (isset($_GET["options"])) {
            $this->options = $_GET["options"];
        }
    }

    public function SetId($id = false) {
        return $this->ids = ($id ? $id : Permalink::Get(1));
    }

    public function SetPage($page = false) {
        return $this->page = ($page ? $page : (Permalink::Get(1) ? Permalink::Get(1) : 1));
    }

    public function SetSearch($search = false) {
        if (isset($_POST["search"])) {
            $this->search = $_POST["search"];
            $this->page = 1;
            System::Set("Redirect", "admin/internal/dashboard/category/1/{$this->search}", "Admin");
            return true;
        }
        return $this->search = ($search ? $search : Permalink::Get(2));
    }

    public function Save() {
        if (isset($_POST["ips_confirm"])) {
            $this->Clean();
            System::Extract($_POST, false, false, "Form");
            $saved = (!$this->ids ? $this->Insert() : $this->Update($this->ids));
            if ($saved) {
                System::Redirect(Core::Remote(System::Get("Redirect", "Admin")));
                Admin::Alert("Registro salvado!");
                return true;
            }
            Admin::Alert("Problemas ao efetuar operação!");
        }
        return false;
    }

    public function Delete() {
        if ($this->ids) {
            $delete = new MySQLDelete();
            $delete->Table("ips_category");
            $delete->In("id", explode(",", $this->ids));
            if ($delete->Go()) {
                System::Redirect(Core::Remote(System::Get("Redirect", "Admin")));
                Admin::Alert("Registro(s) removido(s)!");
                return true;
            }
        }
        Admin::Alert("Problemas ao efetuar operação!");
        return false;
    }

    public function Open() {
        if ($this->ids) {
            $search = new MySQLSearch();
            $search->Table("ips_category");
            $search->Column("*");
            $search->Match("id", $this->ids);
            $search->Limit(1);
            $search = $search->Go();
            if ($search) {
                System::Extract($search[0], null, null, "Form");
                return true;
            }
            Admin::Alert("Registro não encontrado!");
        }
        return false;
    }

    public function Search() {
        $search = new MySQLSearch();
        $search->Table("ips_category");
        $search->Column("id");
        $search->Column("name");
        $search->Column("description");
        if ($this->search) {
            $search->Like("name", "%{$this->search}%");
            $search->Like("description", "%{$this->search}%", "OR");
            System::Set("Search Text", $this->search);
        }
        $search->Order(2);
        $search->limit = $this->Pagination($search->where);
        $search = $search->Go();
        if ($search) {
            return System::Set("Search", $search);
        }
        return false;
    }

    public function Options() {
        $search = new MySQLSearch();
        $search->Table("ips_category");
        $search->Column("id");
        $search->Column("name");
        $search->Order(2);
        if ($this->options) {
            $search->Like("name", "%{$this->options}%");
        }
        System::Set("Options Category", $search->Go());
    }

    private function Pagination($where) {
        $total = new MySQLSearch();
        $total->Table("ips_category");
        $total->Column("COUNT(id)", false, "total");
        $total->where = $where;
        $pagination = System::Pages($total, $this->page, admin_limit);
        if ($pagination) {
            System::Set("Search Pagination", $pagination[1]);
            return $pagination[0];
        }
        return false;
    }

    private function Clean() {
        $clean = new MySQLShow();
        $clean->Table("ips_category");
        $fields = $clean->Columns();
        if ($fields) {
            foreach ($fields as $field) {
                System::Set($field["Field"], false, "Form");
            }
        }
    }

    private function Insert() {
        $save = new MySQLSave();
        $save->Table("ips_category");
        $save->Column("name", System::Get("name", "Form"));
        $save->Column("description", System::Get("description", "Form"));
        if ($save->Go()) {
            return $save->GetID();
        }
        return false;
    }

    private function Update($id) {
        $save = new MySQLSave();
        $save->Table("ips_category");
        $save->Column("name", System::Get("name", "Form"));
        $save->Column("description", System::Get("description", "Form"));
        $save->Match("id", $id);
        if ($save->Go()) {
            return $id;
        }
        return false;
    }

}

?>