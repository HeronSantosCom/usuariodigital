<?php

if (!class_exists('source')) {

    /**
     * Exibe ou redireciona para o aquivo desejado.
     * @name kengoo/kernel/source
     * @author Heron Santos
     * @copyright www.heronsantos.com
     */
    class source {

        /**
         * Detecta qual arquivo a ser aberto.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $open <p>Arquivo a ser aberto, padrão = FALSE</p>
         * @return mixed
         */
        static function get($open = false) {
            if (!$open) {
                $uri = explode(root::uri(), urldecode($_SERVER['REQUEST_URI']), 2);
                if (isset($uri[1])) {
                    $uri = explode("?", $uri[1], 2);
                    $uri = explode("#", $uri[0], 2);
                    $open = $uri[0];
                }
            }
            return self::boot($open);
        }

        /**
         * Abre o arquivo desejado.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $open <p>Arquivo a ser aberto</p>
         * @return mixed
         */
        static private function boot($open) {
            $path = root::path("source/{$open}");
            if (file_exists($path)) {
                $pathinfo = pathinfo($path);
                if (isset($pathinfo['extension'])) {
                    $extensions = $pathinfo['extension'];
                    switch ($extensions) {
                        case "php":
                            ob_start();
                            include $path;
                            return ob_get_clean();
                            break;
                        case "xhtml":
                            return file_get_contents($path);
                            break;
                        case "html":
                            header("Content-type: text/html; charset=" . system_encoding);
                            kernel::set("this", str_replace("source/", "", "/{$path}"), "root");
                            return compose::get(file_get_contents($path));
                            break;
                        default:
                            if (strlen($extensions) > 0) {
                                $mimetype = kernel::mimetype($path, $extensions);
                                if (strlen($mimetype) > 0) {
                                    header("Pragma: public"); // required 
                                    header("Expires: 0");
                                    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                                    header("Cache-Control: private", false); // required for certain browsers 
                                    header("Content-Type: {$mimetype}");
                                    //header("Content-Disposition: attachment; filename=\"" . basename($path) . "\";");
                                    header("Content-Transfer-Encoding: binary");
                                    header("Content-Length: " . filesize($path));
                                    flush();
                                    readfile($path);
                                    exit(0);
                                }
                                return error(400, "The request could not be understood by the server due to malformed syntax.");
                            }
                            break;
                    }
                }
            }
            if (file_exists("{$path}/index.html")) {
                header("Location: " . root::host($open) . "/index.html");
                exit();
            } else {
                if (file_exists(root::path("{$path}.html"))) {
                    header("Location: " . root::host($open) . ".html");
                    exit();
                }
            }
            return error(404, "Could not open file " . root::host($open));
        }

    }

}
?>