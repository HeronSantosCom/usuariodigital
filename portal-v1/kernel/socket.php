<?php

class Socket extends System {

    protected $server, $port, $timeout;
    public $link;

    public function __construct($server, $port = 25, $timeout = 45) {
        $this->server = $server;
        $this->port = $port;
        $this->timeout = $timeout;
    }

//    public function __sleep() {
//        return array('server', 'port', 'timeout');
//    }
//
//    public function __wakeup() {
//        $this->Open();
//    }
//
    public function __destruct() {
        $this->Close();
    }

    public function Open() {
        $this->link = @fsockopen($this->server, $this->port, $errno, $errstr, $this->timeout);
        if ($this->link) {
            parent::Log("Connected at {$this->server}:{$this->port}...", "socket.log");
            socket_set_timeout($this->link, 0, ($this->timeout * 1000));
            return $this->link;
        }
        trigger_error($errstr, E_USER_WARNING);
        return false;
    }

    public function Put($command) {
        if (is_resource($this->link)) {
            $command .= "\r\n";
            $response = fputs($this->link, $command, strlen($command) + 2);
            if ($response) {
                parent::Log(">>>> {$command}", "socket.log");
                return $response;
            }
        }
        return false;
    }

    public function Get($put = false, $break = false) {
        if ($put) {
            if (!$this->Put($put)) {
                return false;
            }
        }
        if (is_resource($this->link)) {
            $continue = true;
            while ($continue) {
                $response[] = $parser = fgets($this->link);
                parent::Log("<<<< {$parser}", "socket.log");
                if (!$break) {
                    if (!(strpos($parser, "\r\n") == false or substr($parser, 3, 1) != ' ')) {
                        $continue = false;
                    }
                } else {
                    if ($parser == $break) {
                        $continue = false;
                    }
                }
            }
            if (isset($response)) {
                return $response;
            }
        }
        return false;
    }

    public function Close() {
        if (is_resource($this->link)) {
            if (fclose($this->link)) {
                parent::Log("Disconnected from {$this->server}:{$this->port}...", "socket.log");
                return true;
            }
        }
        return false;
    }

}

?>
