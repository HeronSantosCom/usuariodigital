<?php

if (!function_exists("reporting")) {

    /**
     * Exibe mensagem de erro
     * @param int $code
     * @param string $message
     * @return null
     */
    function reporting($code = 500, $message = false) {
        if ($message) {
            @ob_clean();
            switch ($code) {
                case 404:
                    $codemsg = "404 Not Found";
                    break;
                case 500:
                default:
                    $codemsg = "500 Internal Server Error";
                    break;
            }
            @header("HTTP/1.0 {$codemsg}");
            @header("Status: {$codemsg}");
            $_SERVER['REDIRECT_STATUS'] = (int) $code;
            $trace = @debug_backtrace();
            if (is_array($trace)) {
                array_pop($trace);
                foreach ($trace as $key => $line) {
                    //if (isset($line["file"]) and !preg_match("#kernel/#", $line["file"])) {
                    if (isset($line["file"])) {
                        $new[] = array("file" => $line["file"], "line" => (isset($line["line"]) ? "<b>: linha {$line["line"]}</b>" : null));
                    }
                }
                $trace = (isset($new) ? $new : null);
            }
            System::Set("Error Code", $code);
            System::Set("Error Code Message", $codemsg);
            System::Set("Error Date", date("r"));
            System::Set("Session ID", @session_id());
            System::Set("Session IP", (isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : null));
            System::Set("Session IP Reverse", (isset($_SERVER["REMOTE_ADDR"]) ? @gethostbyaddr($_SERVER["REMOTE_ADDR"]) : null));
            System::Set("Error Message", $message);
            System::Set("Error Trace", $trace);
            $log[] = $codemsg;
            $log[] = (is_array($message) ? join("\n", $message) : $message);
            if (is_array($trace)) {
                foreach ($trace as $line) {
                    $log[] = strip_tags("{$line['file']} {$line['line']}");
                }
            } // E_USER_ERROR | E_USER_WARNING | E_USER_NOTICE
            System::Log(join("\n", $log), "error.log");
            if (system_error_die) {
                if (system_error_show) {
                    print Interfaces::Show("error");
                }
                unset($_SESSION);
                @session_destroy();
                die(0);
            }
            return true;
        }
        return false;
    }

}
?>
