<?php

if (!class_exists('mysqldelete')) {

    /**
     * Remove um ou mais registro na base de dados MySQL.
     * @name kengoo/kernel/mysql/mysqldelete
     * @author Heron Santos
     * @copyright www.heronsantos.com
     */
    class mysqldelete extends mysql {

        private $database = false;
        private $table = false;
        private $where = false;
        private $result = false;

        public function __construct() {
            parent::__construct();
        }

        /**
         * Seta o banco de dados.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $name <p>Bando de dados</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function database($name) {
            $index = $this->index("database");
            $this->database[$index] = $this->ereaser($name, "`");
            return $this;
        }

        /**
         * Seta a tabela.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $name <p>Tabela</p>
         * @param integer $databasekey <p>Banco de dados (chave de indicação), padrão = 1</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function table($name, $databasekey = 1) {
            $database = (isset($this->database[$databasekey]) ? "{$this->database[$databasekey]}." : $this->ereaser($this->select, "`") . ".");
            $table = $this->ereaser($name, "`");
            $index = $this->index("table");
            $this->table[$index] = "{$database}{$table}";
            return $this;
        }

        /**
         * Seta comparação na condição, igual ( = ) ou diferente ( <> ).
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $column <p>Coluna</p>
         * @param string $value <p>Valor</p>
         * @param bool $inverse <p>FALSE para igual ou TRUE para diferente, padrão = FALSE</p>
         * @param integer $tablekey <p>Tabela (chave de indicação), padrão = 1</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function match($column, $value, $inverse = false, $tablekey = 1) {
            if (!$this->is_function($column)) {
                $table = (isset($this->table[0]) ? "{$this->table[0]}." : null);
                if ($tablekey) {
                    $table = (isset($this->table[$tablekey]) ? "{$this->table[$tablekey]}." : $table);
                }
                $column = $table . $this->ereaser($column, "`", false);
            }
            if (!$this->is_function($value)) {
                $value = (strlen($value) > 0 ? "\"" . mysql_real_escape_string($value, $this->link) . "\"" : "NULL");
            }
            $inverse = ($inverse ? "<>" : "=");
            return $this->where("{$column} {$inverse} {$value}");
        }

        /**
         * Seta comparação na condição, incluso ( IN ) ou não incluso ( NOT IN ).
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $column <p>Coluna</p>
         * @param string $value <p>Valor</p>
         * @param bool $inverse <p>FALSE para incluso ou TRUE para não incluso, padrão = FALSE</p>
         * @param integer $tablekey <p>Tabela (chave de indicação), padrão = 1</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function in($column, $value, $inverse = false, $tablekey = 1) {
            if (is_array($value)) {
                if (!$this->is_function($column)) {
                    $table = (isset($this->table[0]) ? "{$this->table[0]}." : null);
                    if ($tablekey) {
                        $table = (isset($this->table[$tablekey]) ? "{$this->table[$tablekey]}." : $table);
                    }
                    $column = $table . $this->ereaser($column, "`", false);
                }
                foreach ($value as $key => $aux) {
                    if (!$this->is_function($aux)) {
                        $value[$key] = (strlen($aux) > 0 ? "\"" . mysql_real_escape_string($aux, $this->link) . "\"" : "NULL");
                    }
                }
                $value = join(", ", $value);
                $inverse = ($inverse ? "NOT IN" : "IN");
                return $this->where("{$column} {$inverse} ({$value})");
            }
            trigger_error("It is necessary to pass the value in array!", E_USER_WARNING);
            return false;
        }

        /**
         * Seta comparação na condição, aproximado ( LIKE ) ou não aproximado ( NOT LIKE ).
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $column <p>Coluna</p>
         * @param string $value <p>Valor</p>
         * @param bool $inverse <p>FALSE para aproximado ou TRUE para não aproximado, padrão = FALSE</p>
         * @param integer $tablekey <p>Tabela (chave de indicação), padrão = 1</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function like($column, $value, $inverse = false, $tablekey = 1) {
            if (!$this->is_function($column)) {
                $table = (isset($this->table[0]) ? "{$this->table[0]}." : null);
                if ($tablekey) {
                    $table = (isset($this->table[$tablekey]) ? "{$this->table[$tablekey]}." : $table);
                }
                $column = $table . $this->ereaser($column, "`", false);
            }
            if (!$this->is_function($value)) {
                $value = (strlen($value) > 0 ? "\"" . mysql_real_escape_string($value, $this->link) . "\"" : "NULL");
            }
            $inverse = ($inverse ? "NOT LIKE" : "LIKE");
            return $this->where("{$column} {$inverse} {$value}");
        }

        /**
         * Seta condição manualmente.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $conditions <p>Condição</p>
         * @return object <p>Ponteiro da classe, permite concatenação de chamadas</p>
         */
        public function where($conditions) {
            $index = $this->index("where");
            $this->where[$index] = $conditions;
            return $this;
        }

        /**
         * Executa remoção de registro.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @return bool <p>Executando corretamente retorna TRUE, caso contrário retorna FALSE</p>
         */
        public function go() {
            $where = null;
            if ($this->table) {
                $table = join(", ", $this->table);
                if ($this->where) {
                    $where = " WHERE " . join(" AND ", $this->where);
                }
                $query = "DELETE FROM {$table}{$where}";
                $this->result = $this->commit($query);
                if ($this->result["result"]) {
                    if ($this->result["rows"] > 0) {
                        return true;
                    }
                }
                return false;
            }
            trigger_error("Table not defined!", E_USER_NOTICE);
            return false;
        }

        /**
         * Retorna a quantidade de registros afetados.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @return integer <p>Quantidade de registros afetados, caso contrario FALSE</p>
         */
        public function rows() {
            if (isset($this->result["rows"])) {
                return $this->result["rows"];
            }
            return false;
        }

        /**
         * Define ou retorna o ponteiro de um determinado objeto.
         * @author Heron Santos
         * @copyright www.heronsantos.com
         * @param string $object <p>Objeto</p>
         * @param bool $get <p>TRUE retorna o ponteiro atual do objeto, padrão = FALSE</p>
         * @return integer <p>Ponteiro, caso contrario FALSE</p>
         */
        private function index($object, $get = false) {
            if ($get) {
                if (isset($this->index[$object])) {
                    return $this->index[$object];
                }
                return false;
            }
            return $this->index[$object] = (isset($this->index[$object]) ? $this->index[$object] + 1 : 1);
        }

    }

}
?>